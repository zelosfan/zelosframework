package de.zelosfan.framework.Intelligence;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import de.zelosfan.framework.Logging.LogManager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.05.13
 * Time: 13:21
 */
public class PlatformInformation {

    public static boolean isMobile() {
        return (Gdx.app.getType() == Application.ApplicationType.Android) || (Gdx.app.getType() == Application.ApplicationType.iOS);
    }

    public static boolean isDesktop() {
        return (Gdx.app.getType() == Application.ApplicationType.Desktop) || (Gdx.app.getType() == Application.ApplicationType.WebGL) ||
                (Gdx.app.getType() == Application.ApplicationType.Applet);
    }

    public static boolean isWeb() {
        return (Gdx.app.getType() == Application.ApplicationType.Applet) || (Gdx.app.getType() == Application.ApplicationType.WebGL);
    }

    public static long getTotalMemoryHeap() {
        return Gdx.app.getJavaHeap() + Gdx.app.getNativeHeap();
    }

    public static boolean checkVersion(int minimum) {
        return Gdx.app.getVersion() == 0 && Gdx.app.getType() != Application.ApplicationType.Android || Gdx.app.getVersion() >= minimum;
    }

    public static void setFullscreen(boolean val) {
        setFullscreen(val, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public static void setFullscreen(boolean fullscreen, Graphics.DisplayMode displayMode) {
        setFullscreen(fullscreen, displayMode.width, displayMode.height);
    }

    public static void setFullscreen(boolean fullscreen, int width, int height) {
        LogManager.log(PlatformInformation.class, Application.LOG_DEBUG, "(setFullscreen) try to change FullscreenMode to -> " + fullscreen);
        if (fullscreen != Gdx.graphics.isFullscreen()) {
            Gdx.graphics.setDisplayMode(width, height, fullscreen);
            LogManager.log(PlatformInformation.class, Application.LOG_DEBUG, "(setFullscreen) FullscreenMode changed accordingly with width | height -> " + width + " | " + height);
        }
    }

    public static Graphics.DisplayMode getHighestSupportedSettings() {
        Graphics.DisplayMode highestSetting = Gdx.graphics.getDisplayModes()[0];
        for (Graphics.DisplayMode displayMode : Gdx.graphics.getDisplayModes()) {
            if (highestSetting == null) {
                highestSetting = displayMode;
            } else {
                if (highestSetting.width < displayMode.width || highestSetting.height < displayMode.height) {
                    highestSetting = displayMode;
                } else if (highestSetting.height == displayMode.height && highestSetting.width == displayMode.width && highestSetting.refreshRate < displayMode.refreshRate) {
                    highestSetting = displayMode;
                }
            }
        }
        return highestSetting;
    }

}
