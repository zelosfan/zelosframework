package de.zelosfan.framework.GameState;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Input.InputPreProcessor;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.Rendermanager;

import java.util.LinkedList;
import java.util.Stack;


/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.05.13
 * Time: 16:10
 */
public class GameStateManager {
    private static GameStateManager instance = new GameStateManager();

    private Stack<GameState> stateStack = new Stack<>();

    public GameStateManager() {
    }

    public static GameStateManager getGameStateManager() {
        return instance;
    }

    public GameState getCurrentGameState() {
        if (stateStack.isEmpty()) return null;

        GameState gameState = stateStack.peek();
        if (gameState == null) {
            throw new NullPointerException("Current GameState in GameStateManager is NULL");
        }

        return gameState;
    }

    public void changeToGameState(GameState gameState) {
        while (!stateStack.empty()) {
            GameState currentState = stateStack.pop();
            currentState.onDeactivate();
            currentState.dispose();
        }
        stateStack.push(gameState);

        gameState.onActivate();
        Gdx.input.setInputProcessor(gameState.getInputMultiplexer());
    }

    public void pushGameState(GameState gameState) {
        if (stateStack.peek() != null) {
            stateStack.peek().onDeactivate();
        }

        stateStack.push(gameState);
        gameState.onActivate();
        Gdx.input.setInputProcessor(gameState.getInputMultiplexer());
    }

    public void popGameState() {
        stateStack.pop().onDeactivate();
        stateStack.peek().onActivate();
        Gdx.input.setInputProcessor(stateStack.peek().getInputMultiplexer());
    }

    public void tick() {
        if (stateStack.isEmpty()) return;
        getCurrentGameState().tick();
    }

    public void render() {
        getCurrentGameState().render();
    }

    public void dispose() {
        while (!stateStack.empty()) {
            stateStack.pop().dispose();
        }
    }
}
