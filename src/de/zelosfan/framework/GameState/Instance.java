package de.zelosfan.framework.GameState;

import com.badlogic.gdx.InputProcessor;
import de.zelosfan.framework.Rendering.Rendermanager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.05.13
 * Time: 15:34
 */
public interface Instance extends InputProcessor {
    void onActivate();

    void onDeactivate();

    void render();

    void tick();
}
