package de.zelosfan.framework.GameState;

import com.badlogic.gdx.*;
import de.zelosfan.framework.Input.InputPreProcessor;
import de.zelosfan.framework.Logging.LogManager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.05.13
 * Time: 15:36
 */
public abstract class GameState {

    public final boolean inputPreProcessor;

    protected Instance[] instances;

    public GameState() {
        inputPreProcessor = true;
        instances = new Instance[0];
    }

    public GameState(boolean _inputPreProcessor, Instance... _instances) {
        inputPreProcessor = _inputPreProcessor;
        instances = _instances;
    }

    //prevGameState: State to fetch instances from
    public GameState(boolean _inputPreProcessor, GameState prevGameState) {
        this(_inputPreProcessor, prevGameState.instances);
    }

    public void render() {
        for (Instance instance: instances) {
            instance.render();
        }
    }

    public InputProcessor getInputMultiplexer() {
        if (inputPreProcessor) {
            return new InputPreProcessor(buildInputMultiplexer());
        } else {
            return buildInputMultiplexer();
        }
    }

    public void onActivate() {
        LogManager.log(GameState.class, Application.LOG_DEBUG, "(onActivate) Activating gamestate -> " + getClass().getSimpleName());
        for (Instance instance: instances) {
            instance.onActivate();
        }
    }

    public void onDeactivate() {
        LogManager.log(GameState.class, Application.LOG_DEBUG, "(onDeactivate) Deactivating gamestate -> " + getClass().getSimpleName());
        for (Instance instance: instances) {
            instance.onDeactivate();
        }
    }

    public void tick() {
        for (Instance instance: instances) {
            instance.tick();
        }
    }

    public void dispose() {
        for (int i = 0; i < instances.length; i++) {
            instances[i] = null;
        }
        instances = null;
    }

    public Instance[] getInstances() {
        return instances;
    }

    protected abstract InputMultiplexer buildInputMultiplexer();
}
