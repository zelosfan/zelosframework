package de.zelosfan.framework.Physics;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.09.13
 * Time: 13:25
 */
public interface FixtureCallback {
    void fixtureCallback(Fixture fixture);
}
