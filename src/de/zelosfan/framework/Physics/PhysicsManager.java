package de.zelosfan.framework.Physics;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;
import de.zelosfan.framework.Collision.CollisionFilter;
import de.zelosfan.framework.Collision.CollisionHandler;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.Rendermanager;

import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.06.13
 * Time: 05:30
 */
public class PhysicsManager implements Disposable, QueryCallback {
    public static float STEPS_PER_TICK = 1 / 100;

    public static int VelocityIterations = 6;
    public static int PositionIterations = 2;

    public static float BOX2D_TO_GAME_X = 1;
    public static float GAME_TO_BOX2D_X = 1;

    public static float BOX2D_TO_GAME_Y = 1;
    public static float GAME_TO_BOX2D_Y = 1;

    public static OrthographicCamera physicsCamera;

    public static LinkedList<Body> enableList = new LinkedList<>();
    public static LinkedList<Body> disableList = new LinkedList<>();

    public static LinkedList<Joint> garbageJoint = new LinkedList<>();
    public static LinkedList<Body> garbageBody = new LinkedList<>();
    public static LinkedList<Fixture> garbageFixture = new LinkedList<>();

    FixtureCallback callback;
    boolean getOnlyFirstFixture = false;
    float testPointX = 0;
    float testPointY = 0;

    public static World world;

    public PhysicsManager(Vector2 _gravity, float box2d_to_game_ratio, float steps_per_tick) { // creates default collisionHandler and Filter
        this(_gravity, new CollisionHandler(), new CollisionFilter(), box2d_to_game_ratio, steps_per_tick);
    }

    public PhysicsManager(Vector2 _gravity, ContactListener contactListener, ContactFilter contactFilter, float box2d_to_game_ratio, float steps_per_tick) {
        world = new World(_gravity, true);

        if (contactListener != null) {
            world.setContactListener(contactListener);
        } else {
            LogManager.log(this.getClass(), Application.LOG_INFO, "(constructor) Given contactListener is not valid (NULL)");
        }

        if (contactFilter != null) {
            world.setContactFilter(contactFilter);
        } else {
            LogManager.log(this.getClass(), Application.LOG_INFO, "(constructor) Given contactFilter is not valid (NULL)");
        }

        if (box2d_to_game_ratio <= 0) {
            LogManager.log(this.getClass(), Application.LOG_ERROR, "(constructor) Box2dToGameRatio is invalid  -> " + box2d_to_game_ratio);
        } else {
            BOX2D_TO_GAME_X = box2d_to_game_ratio;
            GAME_TO_BOX2D_X = 1 / BOX2D_TO_GAME_X;

            BOX2D_TO_GAME_Y = box2d_to_game_ratio * ((float) Gdx.graphics.getWidth() / Gdx.graphics.getHeight());
            GAME_TO_BOX2D_Y = 1 / BOX2D_TO_GAME_Y;
        }

        if (steps_per_tick > 0) {
            STEPS_PER_TICK = steps_per_tick;
        } else {
            LogManager.log(this.getClass(), Application.LOG_ERROR, "(constructor) stepsPerTick is invalid  -> " + steps_per_tick);
        }

        physicsCamera = new OrthographicCamera(GAME_TO_BOX2D_X, GAME_TO_BOX2D_Y);
    }

    public void getFixtureFromPoint(FixtureCallback fixtureCallback, float x, float y, boolean getOnlyFirst) {
        callback = fixtureCallback;
        getOnlyFirstFixture = getOnlyFirst;
        testPointX = x;
        testPointY = y;
        world.QueryAABB(this, x, y, x, y);
    }

    public void tick() {
        world.step(STEPS_PER_TICK, VelocityIterations, PositionIterations);
        cleanUp();
    }

    public static float getBox2DXFromAbsoluteScreenX(float screenX) {
        return Rendermanager.CURRENT_CAMPOS_X + (screenX / Gdx.graphics.getWidth()) * GAME_TO_BOX2D_X;
    }

    public static float getBox2DYFromAbsoluteScreenY(float screenY) {
        return Rendermanager.CURRENT_CAMPOS_Y + (screenY / Gdx.graphics.getHeight()) * GAME_TO_BOX2D_Y;
    }


    public static float getBox2DXFromScreenX(float screenX) {
        return Rendermanager.CURRENT_CAMPOS_X + screenX * GAME_TO_BOX2D_X;
    }

    public static float getBox2DYFromScreenY(float screenY) {
        return Rendermanager.CURRENT_CAMPOS_Y + screenY * GAME_TO_BOX2D_Y;
    }

    public boolean cleanUp() {
        if (garbageJoint.size() <= 0 && garbageBody.size() <= 0 && garbageFixture.size() <= 0 && enableList.size() <= 0 && disableList.size() <= 0) {
            return false;
        }

        for (Body body : disableList) {
            body.setActive(false);
        }
        for (Body body : enableList) {
            body.setActive(true);
        }

        for (Joint joint : garbageJoint) {
            world.destroyJoint(joint);
        }
        for (Fixture fixture : garbageFixture) {
            fixture.getBody().destroyFixture(fixture);
        }
        for (Body body : garbageBody) {
            world.destroyBody(body);
        }
        enableList.clear();
        disableList.clear();
        garbageJoint.clear();
        garbageFixture.clear();
        garbageBody.clear();
        return true;
    }

    public void resize(int width, int height) {
        BOX2D_TO_GAME_X = 1f / (width / Gdx.graphics.getPpcX());
        GAME_TO_BOX2D_X = 1f / BOX2D_TO_GAME_X;

        BOX2D_TO_GAME_Y = 1f / (width / Gdx.graphics.getPpcX()) * ((float) width / height);
        GAME_TO_BOX2D_Y = 1f / BOX2D_TO_GAME_Y;

        physicsCamera.viewportWidth = GAME_TO_BOX2D_X;
        physicsCamera.viewportHeight = GAME_TO_BOX2D_Y;
        physicsCamera.update();
    }


    @Override
    public void dispose() {
        world.dispose();
        garbageJoint.clear();
        garbageFixture.clear();
        garbageBody.clear();
    }

    @Override
    public boolean reportFixture(Fixture fixture) {
        if (fixture.testPoint(testPointX, testPointY)) {
            callback.fixtureCallback(fixture);
            if (getOnlyFirstFixture) {
                return false;
            }
        }

        return true;
    }
}
