package de.zelosfan.framework.EventTrigger;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 08.07.13
 * Time: 11:31
 */
public class EventTrigger {
    Triggerable affected;
    boolean active = true;
    public EventTrigger(Triggerable _affected) {
        affected = _affected;
    }

    public void trigger(String event, Object... objects) {
        if (!active) return;

        affected.onEvent(event, objects);
    }

    public void disable() {
        active = false;
    }

    public void enable() {
        active = true;
    }
}
