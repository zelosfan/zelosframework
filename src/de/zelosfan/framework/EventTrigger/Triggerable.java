package de.zelosfan.framework.EventTrigger;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 08.07.13
 * Time: 11:30
 */
public interface Triggerable {
    void onEvent(String event, Object... obj);
}
