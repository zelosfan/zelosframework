package de.zelosfan.framework.EventTrigger;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 08.07.13
 * Time: 11:34
 */
public class TimeTrigger extends EventTrigger{
    public int tickCount = 0;
    public int duration;
    public boolean repeat;
    public String event;

    public TimeTrigger(Triggerable _affected, int _duration, boolean _repeat, String _event) {
        super(_affected);
        duration = _duration;
        repeat = _repeat;
        event = _event;
    }

    public TimeTrigger(Triggerable _affected, int _duration, boolean _repeat) {
        this(_affected, _duration, _repeat, "onTimeTrigger");
    }

    public void tick() {
        if (!active) return;
        tickCount++;

        if (tickCount == duration) {
            trigger(event, tickCount);

            if (repeat) {
                duration = duration * 2;
            } else {
                active = false;
            }
        }
    }


}
