package de.zelosfan.framework.Input;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Logging.LogManager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 03.06.13
 * Time: 17:16
 */
public class KeyMap {

    private static KeyMap keys;

    private static void initialize(Preferences preferences, String _eventListLocation) {
        keys = new KeyMap(preferences, _eventListLocation);
    }

    public static KeyMap getInstance() {
        return keys;
    }

    public ObjectMap<Integer, String> keyMap = new ObjectMap<>();
    public ObjectMap<String, Integer> eventList = new ObjectMap<>();
    private final String EVENTLIST_LOCATION;

    public KeyMap(Preferences preferences) {
        this(preferences, "keys.stg");
    }

    public KeyMap(Preferences preferences, String _eventListLocation) {
        EVENTLIST_LOCATION = _eventListLocation;
        loadEventlist(EVENTLIST_LOCATION);
        loadFromPreferences(preferences);
    }

    public void loadEventlist() {
        loadEventlist(EVENTLIST_LOCATION);
    }

    public void loadEventlist(String eventListLocation) {
        JsonReader json = new JsonReader();
        JsonValue list = json.parse(Gdx.files.internal(eventListLocation));

        for (JsonValue value = list.getChild("events"); value != null; value = value.next()) {
            eventList.put(value.getString("event"), value.getInt("key"));
        }
    }

    public boolean loadFromPreferences(Preferences preferences) {
        if (preferences == null) {
            LogManager.log(this.getClass(), Application.LOG_ERROR, "(PrefLoad) Preferences is not valid (null)");
            return false;
        }
        if (eventList.size <= 0) {
            LogManager.log(this.getClass(), Application.LOG_ERROR, "(PrefLoad) No events in eventList (not loaded?)");
            return false;
        }
        for (String str : eventList.keys()) {
            keyMap.put(eventList.get(str), preferences.getString(eventList.get(str).toString(), str));
        }
        return true;
    }

    public boolean flushToPreferences(Preferences preferences) {
        if (preferences == null) {
            LogManager.log(this.getClass(), Application.LOG_ERROR, "(PrefFlush) Preferences is not valid (null)");
            return false;
        }

        for (Integer integer : keyMap.keys()) {
            preferences.putString(integer.toString(), keyMap.get(integer));
        }
        preferences.flush();
        return true;
    }

}
