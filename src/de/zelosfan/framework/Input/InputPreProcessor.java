package de.zelosfan.framework.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 22.05.13
 * Time: 22:10
 *
 * Desc: Changes Input Orientation from Y-Down to Y-Up
 */
public class InputPreProcessor implements InputProcessor {
    InputProcessor realInput;
    boolean active;

    public InputPreProcessor(InputProcessor realInputProcessor) {
        if (realInputProcessor != null) {
            realInput = realInputProcessor;
            active = true;
        } else {
            active = false;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return active && realInput.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return active && realInput.keyUp(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return active && realInput.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return active && realInput.touchDown(screenX, Gdx.graphics.getHeight() - screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return active && realInput.touchUp(screenX, Gdx.graphics.getHeight() - screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return active && realInput.touchDragged(screenX, Gdx.graphics.getHeight() - screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return active && realInput.mouseMoved(screenX, Gdx.graphics.getHeight() - screenY);
    }

    @Override
    public boolean scrolled(int amount) {
        return active && realInput.scrolled(amount);
    }
}
