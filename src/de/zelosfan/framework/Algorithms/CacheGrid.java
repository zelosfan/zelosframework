package de.zelosfan.framework.Algorithms;


/**
 * User: Simon "Zelosfan" Herfert
 * Date: 15.01.14
 * Time: 11:07
 */
public abstract class CacheGrid<E extends CachedPathable> {

    public E[][][] neighbors;
    public final int width, height;

    public CacheGrid (int _width, int _height) {
        width =_width;
        height = _height;
    }

}
