package de.zelosfan.framework.Algorithms;

import com.badlogic.gdx.utils.ObjectMap;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 15.05.13
 * Time: 06:36
 */
public class AStar {

    public static LinkedList<Pathable> getAStarPath(Pathable[][] nodes, Pathable start, Pathable goal, int standardWaycost, boolean allowVertical) {
        ArrayList<Pathable> closedList = new ArrayList<>();
        LinkedList<Pathable> openList = new LinkedList<>();

        ObjectMap<Pathable, Pathable> came_from = new ObjectMap<>();

        ObjectMap<Pathable, Integer> g_score = new ObjectMap<>(nodes.length + nodes[0].length);
        ObjectMap<Pathable, Integer> f_score = new ObjectMap<>(nodes.length + nodes[0].length);

        g_score.put(start, 0);

        if (allowVertical) {
            f_score.put(start, (int) (g_score.get(start) + Math.abs(start.getPositionV2().dst(goal.getPositionV2()))) * standardWaycost);
        } else {
            f_score.put(start, (int) (g_score.get(start) + Math.abs(start.getPositionV2().x - goal.getPositionV2().x) + Math.abs(start.getPositionV2().y - goal.getPositionV2().y)) * standardWaycost);
        }

        openList.add(start);

        while (openList.size() != 0) {

            Pathable current = openList.getFirst();
            int lowestscore = f_score.get(openList.getFirst());
            for (Pathable node : openList) {
                if (f_score.get(node) < lowestscore) {
                    current = node;
                    lowestscore = f_score.get(node);
                }
            }


            if (current.equals(goal)) {
                return reconstructPath(came_from, goal);
            }

            openList.remove(current);
            closedList.add(current);

            for (int i = -1; i <= 1; i++) {
                for (int l = -1; l <= 1; l++) {
                    if (current.getPositionV2().x + i < nodes.length && current.getPositionV2().y + l < nodes[0].length && current.getPositionV2().x + i >= 0 && current.getPositionV2().y + l >= 0) {
                        if (current.isWalkable(current.equals(start))) {
                            if (Math.abs(i) != Math.abs(l) || allowVertical) {
                                Pathable neighbor = nodes[((int) current.getPositionV2().x + i)][((int) current.getPositionV2().y + l)];
                                int additionalResistance = neighbor.getWaycost();

                                int heuristic;
                                if (allowVertical) {
                                    heuristic = (int) Math.abs(current.getPositionV2().dst(neighbor.getPositionV2()) * standardWaycost);
                                } else {
                                    heuristic = (int) (g_score.get(start) + Math.abs(start.getPositionV2().x - goal.getPositionV2().x) + Math.abs(start.getPositionV2().y - goal.getPositionV2().y)) * standardWaycost;
                                }

                                int tentative_g_score = g_score.get(current) + heuristic;


                                if (closedList.contains(neighbor)) {
//                                    if (tentative_g_score >= g_score.get(neighbor)) {
                                    continue;
//                                    }
                                }

                                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                                    came_from.put(neighbor, current);
                                    g_score.put(neighbor, tentative_g_score + additionalResistance);
                                    f_score.put(neighbor, g_score.get(neighbor) + heuristic);

                                    if (!openList.contains(neighbor)) {
                                        openList.add(neighbor);
                                    }

                                }

                            }
                        } else {
                            closedList.add(current);
                        }
                    }
                }
            }

        }
        return null;

    }

    public static LinkedList<Pathable> reconstructPath(ObjectMap<Pathable, Pathable> came_from, Pathable current) {
        if (came_from.get(current) != null) {
            LinkedList<Pathable> p = reconstructPath(came_from, came_from.get(current));
            p.add(current);
            return p;
        } else {
            LinkedList<Pathable> p = new LinkedList<>();
            p.add(current);
            return p;
        }
    }
}
