package de.zelosfan.framework.Algorithms;

import com.badlogic.gdx.utils.ObjectMap;
import sun.misc.Cache;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 15.01.14
 * Time: 11:36
 *
 * AStar modified to work with a cached grid of interconnected tiles
 * supports clearanceSize (different unit sizes can calculate different paths)
 */
public class AStarCached {

    public static float HEURISTIC_VERTICAL_MULTIPLICATOR = 0.9f; //Bonus going vertical

    public static LinkedList<CachedPathable> getAStarPath(CacheGrid grid, CachedPathable start, CachedPathable goal, int clearanceSize) {
        ArrayList<CachedPathable> closedList = new ArrayList<>();
        ArrayList<CachedPathable> openList = new ArrayList<>();

        ObjectMap<CachedPathable, CachedPathable> came_from = new ObjectMap<>();

        ObjectMap<CachedPathable, Integer> g_score = new ObjectMap<>(grid.width * grid.height);
        ObjectMap<CachedPathable, Integer> f_score = new ObjectMap<>(grid.width * grid.height);

        g_score.put(start, 0);
        f_score.put(start, (g_score.get(start) + Math.abs(start.getX() - goal.getX()) + Math.abs(start.getY() - goal.getY())));

        openList.add(start);

        while (openList.size() != 0) {

            CachedPathable current = openList.get(0);
            int lowestscore = f_score.get(openList.get(0));
            for (CachedPathable node : openList) {
                if (f_score.get(node) < lowestscore) {
                    current = node;
                    lowestscore = f_score.get(node);
                }
            }

            if (current.equals(goal)) {
                return reconstructPath(came_from, goal);
            }

            openList.remove(current);
            closedList.add(current);

            for (CachedPathable neighbor: grid.neighbors[current.getX()][current.getY()]) {

                int diffX = current.getX() - neighbor.getX();
                int diffY = current.getY() - neighbor.getY();
                boolean vertical = false;

                if (diffX != 0 && diffY != 0) {
                    vertical = true;
                }


                if (neighbor == null) continue;
                int additionalResistance = neighbor.getWaycost();

                int heuristic = Math.abs(current.getX() - neighbor.getX()) + Math.abs(current.getY() - neighbor.getY());

                //if it's in vertical position declare it being slightly faster
                if (vertical) {
                    heuristic *= HEURISTIC_VERTICAL_MULTIPLICATOR;
                }

                int tentative_g_score = g_score.get(current) + heuristic;


                if (closedList.contains(neighbor)) {
//                                    if (tentative_g_score >= g_score.get(neighbor)) {
                    continue;
//                                    }
                }

                if (!openList.contains(neighbor) || tentative_g_score < g_score.get(neighbor)) {
                    came_from.put(neighbor, current);
                    g_score.put(neighbor, tentative_g_score + additionalResistance);
                    f_score.put(neighbor, g_score.get(neighbor) + heuristic);

                    if (vertical) {
                        int min = grid.neighbors[current.getX()][current.getY()][0].getClearance();
                        if (grid.neighbors[neighbor.getX()][current.getY()][0].getClearance() < min) {
                            min = grid.neighbors[neighbor.getX()][current.getY()][0].getClearance();
                        }
                        if (grid.neighbors[current.getX()][neighbor.getY()][0].getClearance() < min) {
                            min = grid.neighbors[current.getX()][neighbor.getY()][0].getClearance();
                        }

                        if (!openList.contains(neighbor) && min >= clearanceSize) {
                            openList.add(neighbor);
                        }
                    } else {
                        if (!openList.contains(neighbor) && neighbor.getClearance() >= clearanceSize) {
                            openList.add(neighbor);
                        }
                    }

                }

            }
//                            closedList.add(current);
        }
        return null;

    }

    public static LinkedList<CachedPathable> reconstructPath(ObjectMap<CachedPathable, CachedPathable> came_from, CachedPathable current) {
        if (came_from.get(current) != null) {
            LinkedList<CachedPathable> p = reconstructPath(came_from, came_from.get(current));
            p.add(current);
            return p;
        } else {
            LinkedList<CachedPathable> p = new LinkedList<>();
            p.add(current);
            return p;
        }
    }

    public static int getTotalWaycost(LinkedList<CachedPathable> way) {
        int waycost = 0;
        for (CachedPathable cachedPathable: way) {
            waycost += cachedPathable.getWaycost();
        }
        return waycost;
    }
}
