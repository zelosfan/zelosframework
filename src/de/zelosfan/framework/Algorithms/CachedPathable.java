package de.zelosfan.framework.Algorithms;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 15.01.14
 * Time: 11:06
 */
public interface CachedPathable {
    int getX();
    int getY();

    int getClearance();

    int getWaycost();
}
