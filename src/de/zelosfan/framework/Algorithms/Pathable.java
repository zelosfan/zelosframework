package de.zelosfan.framework.Algorithms;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 15.05.13
 * Time: 06:42
 */
public interface Pathable {
    Vector2 getPositionV2();

    int getWaycost();

    boolean isWalkable(boolean isOrigin);
}
