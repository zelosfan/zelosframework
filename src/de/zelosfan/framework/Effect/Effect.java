package de.zelosfan.framework.Effect;

import de.zelosfan.framework.Rendering.Rendermanager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.07.13
 * Time: 08:54
 *
 * Desc:
 *  Add extra render calls(on a per class basis) to gameObjects or other stuff
 */
public abstract class Effect {
    protected boolean finished = false;
    protected Modifieable object;
    protected long tickCount = 1;

    public Effect(Modifieable _object) {
        object = _object;
    }

    public void tick() {
        if (finished) {
            return;
        }

        tickCount++;
        if (tickCount == Long.MAX_VALUE) {
            tickCount = 1;
        }
    }

    public void render(Rendermanager rendermanager) {

    }
}
