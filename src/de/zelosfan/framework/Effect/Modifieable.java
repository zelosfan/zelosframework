package de.zelosfan.framework.Effect;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.07.13
 * Time: 08:56
 */
public interface Modifieable {
    void set(int id, float value);
    float get(int id);
}
