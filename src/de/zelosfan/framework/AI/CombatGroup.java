package de.zelosfan.framework.AI;

import com.badlogic.gdx.Game;
import de.zelosfan.framework.GameObject.GameObject;

import java.util.ArrayList;

/**
 * Created by Zelos on 23.04.2015.
 */
public class CombatGroup {

    public static int ID_COUNTER = 0;

    public final int uid;
    protected ArrayList<GameObject> gameObjects;

    public CombatGroup(GameObject... gameObjects) {
        this.gameObjects = new ArrayList<>();

        for (GameObject gameObject: gameObjects) {
            this.gameObjects.add(gameObject);
        }

        uid = ID_COUNTER;
        ID_COUNTER++;
    }

    public void addGameObject(GameObject gameObject) {
        gameObjects.add(gameObject);
        sendMessage("addGameObject", this, gameObject);
    }

    public void removeGameObject(GameObject gameObject) {
        gameObjects.remove(gameObject);
        sendMessage("removeGameObject", this, gameObject);
    }

    public void sendMessage(String msg, Object... objects) {
        for (GameObject gameObject: gameObjects) {
            gameObject.receive(msg, this, objects);
        }
    }

    public void receive(String event, Object... objects) {

    }

    public void tick() {

    }
}
