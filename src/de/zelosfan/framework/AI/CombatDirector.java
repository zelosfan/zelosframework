package de.zelosfan.framework.AI;

import java.util.ArrayList;

/**
 * Created by Zelos on 23.04.2015.
 */
public class CombatDirector {

    protected ArrayList<CombatGroup> combatGroups;

    public CombatDirector() {
        combatGroups = new ArrayList<>(4);
    }

    public void addCombatGroup(CombatGroup combatGroup) {
        combatGroups.add(combatGroup);
    }

    public void removeCombatGroup(CombatGroup combatGroup) {
        combatGroups.remove(combatGroup);
    }

    public void tick() {
        for (CombatGroup combatGroup: combatGroups) {
            combatGroup.tick();
        }
    }



}
