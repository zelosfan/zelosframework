package de.zelosfan.framework.AI;

import de.zelosfan.framework.GameObject.GameObject;
import de.zelosfan.framework.Util.RectangleM;

import java.util.ArrayList;

/**
 * Created by Zelos on 23.04.2015.
 */
public class CombatGroupAreaRestricted extends CombatGroup{

    public RectangleM area;

    private ArrayList<GameObject> observedList;

    public CombatGroupAreaRestricted(RectangleM area, ArrayList<GameObject> _observedList, GameObject... gameObjects) {
        super(gameObjects);
        this.area = area;
        observedList = _observedList;
    }



    @Override
    public void tick() {
        super.tick();


        for (GameObject gameObject: gameObjects) {
            if (!area.contains(gameObject.getPosition().x, gameObject.getPosition().y)) {
                removeGameObject(gameObject);
            }
        }

        for (GameObject gameObject: observedList) {
            if (area.contains(gameObject.getPosition().x, gameObject.getPosition().y)) {
                addGameObject(gameObject);
            }
        }

    }
}
