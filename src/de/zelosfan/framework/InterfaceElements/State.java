package de.zelosfan.framework.InterfaceElements;

/**
 * Created by Zelos on 31.05.2015.
 */
public enum State {
    NORMAL, HOVERED, PRESSED
}
