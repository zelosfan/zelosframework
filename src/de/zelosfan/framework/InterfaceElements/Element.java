package de.zelosfan.framework.InterfaceElements;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import javax.imageio.ImageReader;

/**
 * Created by Zelos on 31.05.2015.
 */
public abstract class Element {

    protected boolean visable = true;
    protected boolean disabled = false;

    protected State state = State.NORMAL;

    protected TextureRegion normalImage, hoverImage, clickedImage;

    public Element(TextureRegion _normalImage, TextureRegion _hoverImage, TextureRegion _clickedImage) {
        normalImage = _normalImage;
        hoverImage = _hoverImage;
        clickedImage = _clickedImage;
    }

    public TextureRegion getTexture(State state) {
        switch (state) {
            case NORMAL:
                return normalImage;

            case HOVERED:
                return hoverImage;

            case PRESSED:
                return clickedImage;
        }

        return null;
    }

    public boolean isVisable() {
        return visable;
    }

    public void setVisable(boolean visable) {
        this.visable = visable;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public final boolean touchDown(int mX, int mY) {
        if (positionInArea(mX, mY) && !(isDisabled() || !isVisable())) {
            onMouseDown();
            return true;
        } else {
            return false;
        }
    }

    public final boolean touchUp(int mX, int mY) {
        if (positionInArea(mX, mY) && !(isDisabled() || !isVisable())) {
            onMouseUp();
            return true;
        } else {
            return false;
        }
    }

    public final boolean mouseMove(int mX, int mY) {
        if (positionInArea(mX, mY) && !(isDisabled() || !isVisable())) {
            onHover();
            return true;
        } else {
            onHoverExit();
            return false;
        }
    }

    //confirmed events
    public void onMouseDown() {
        switch (state) {
            case HOVERED:
            case NORMAL:
                state = State.PRESSED;
                break;
        }
    }

    public void onMouseUp() {
        switch (state) {
            case HOVERED:
            case PRESSED:
                state = State.HOVERED;
                break;
        }
    }

    public void onHover() {
        switch (state) {
            case NORMAL:
                state = State.HOVERED;
                break;
        }
    }

    public void onHoverExit() {
        switch (state) {
            case HOVERED:
                state = State.NORMAL;
                break;

            case PRESSED:
                state = State.NORMAL;
                break;
        }
    }
    //**********************

    public abstract void draw();

    public abstract int getX();

    public abstract int getY();

    public abstract boolean positionInArea(int mX, int mY);

}
