package de.zelosfan.framework.InterfaceElements.advanced;

import com.badlogic.gdx.InputProcessor;

import java.util.ArrayList;

/**
 * Created by Zelos on 15.06.2015.
 */
public abstract class UIElement implements InputProcessor {

    protected ArrayList<UIElementRelation> children = new ArrayList<>();

    protected int x, y;
    protected int width, height;

    public UIElement(int _x, int _y, int _width, int _height) {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
    }

    public void render() {
        render(0, 0);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /*
        pos: absolute starting renderPosition
         */
    public void render(int posX, int posY) {
        UIRenderManager uiRenderManager = new UIRenderManager(posX, posY, this);
        render(uiRenderManager);

        for (UIElementRelation uiElementRelation: children) {
            uiElementRelation.render(posX + x, posY + y, width, height);
        }
    }

    protected abstract void render(UIRenderManager uiRenderManager);

    public void addChild(UIElementRelation child) {
        children.add(child);
    }

    public void addChild(UIElement uiElement) {
        children.add(new UIElementRelation(0, 0, false, uiElement));
    }

    @Override
    public boolean keyDown(int keycode) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.uiElement.keyDown(keycode)) {
                return true;
            }
        }

        return keyDowned(keycode);
    }

    public abstract boolean keyDowned(int keycode);

    @Override
    public boolean keyUp(int keycode) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.uiElement.keyUp(keycode)) {
                return true;
            }
        }

        return keyUped(keycode);
    }

    public abstract boolean keyUped(int keycode);

    @Override
    public boolean keyTyped(char character) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.uiElement.keyTyped(character)) {
                return true;
            }
        }

        return keyTypeded(character);
    }

    public abstract boolean keyTypeded(char character);

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.relativePosition) {
                if (uiElementRelation.uiElement.touchDown(Math.round(screenX - x - uiElementRelation.positionX * width), Math.round(screenY - y - uiElementRelation.positionY * height), pointer, button)) {
                    return true;
                }
            } else {
                if (uiElementRelation.uiElement.touchDown(Math.round(screenX - x - uiElementRelation.positionX), Math.round(screenY - y - uiElementRelation.positionY), pointer, button)) {
                    return true;
                }
            }
        }

        return touchDown(screenX - x, screenY - y, button);
    }

    public abstract boolean touchDown(int screenX, int screenY, int button);

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.relativePosition) {
                if (uiElementRelation.uiElement.touchUp(Math.round(screenX - x - uiElementRelation.positionX * width), Math.round(screenY - y - uiElementRelation.positionY * height), button)) {
                    return true;
                }
            } else {
                if (uiElementRelation.uiElement.touchUp(Math.round(screenX - x - uiElementRelation.positionX), Math.round(screenY - y - uiElementRelation.positionY), button)) {
                    return true;
                }
            }
        }

        return touchUp(screenX - x, screenY - y, button);
    }

    public abstract boolean touchUp(int screenX, int screenY, int button);

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.relativePosition) {
                if (uiElementRelation.uiElement.touchDragged(Math.round(screenX - x - uiElementRelation.positionX * width), Math.round(screenY - y - uiElementRelation.positionY * height))) {
                    return true;
                }
            } else {
                if (uiElementRelation.uiElement.touchDragged(Math.round(screenX - x - uiElementRelation.positionX), Math.round(screenY - y - uiElementRelation.positionY))) {
                    return true;
                }
            }
        }

        return touchDragged(screenX - x, screenY - y);
    }

    public abstract boolean touchDragged(int screenX, int screenY);

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.relativePosition) {
                if (uiElementRelation.uiElement.mouseMoveded(Math.round(screenX - x - uiElementRelation.positionX * width), Math.round(screenY - y - uiElementRelation.positionY * height))) {
                    return true;
                }
            } else {
                if (uiElementRelation.uiElement.mouseMoveded(Math.round(screenX - x - uiElementRelation.positionX), Math.round(screenY - y - uiElementRelation.positionY))) {
                    return true;
                }
            }
        }

        return  mouseMoveded(screenX - x, screenY - y);
    }

    public abstract boolean mouseMoveded(int screenX, int screenY);

    @Override
    public boolean scrolled(int amount) {
        for (UIElementRelation uiElementRelation: children) {
            if (uiElementRelation.uiElement.scrolled(amount)) {
                return true;
            }
        }

        return scrolleded(amount);
    }

    public abstract boolean scrolleded(int amount);

    public void tick() {

    }
}
