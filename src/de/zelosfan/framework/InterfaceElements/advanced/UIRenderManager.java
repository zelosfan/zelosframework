package de.zelosfan.framework.InterfaceElements.advanced;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Rendering.Rendermanager;

/**
 * Created by Zelos on 15.06.2015.
 */
public class UIRenderManager {

    public static Rendermanager rendermanager = null;

    private int absoluteX, absoluteY;
    private UIElement element;

    public UIRenderManager(int absoluteX, int absoluteY, UIElement element) {
        this.absoluteX = absoluteX;
        this.absoluteY = absoluteY;
        this.element = element;
    }

    public int getAbsoluteX() {
        return absoluteX + element.getX();
    }

    public int getAbsoluteY() {
        return absoluteY + element.getY();
    }

    public int getWidth() {
        return element.width;
    }

    public int getHeight() {
        return element.height;
    }

    //return: get absolute to relative to size within
    public int getRelativePosX(float relativeX) {
        return Math.round(getAbsoluteX() + getWidth() * relativeX);
    }

    public int getRelativePosY(float relativeY) {
        return Math.round(getAbsoluteY() + getHeight() * relativeY);
    }

    public void drawTexture(TextureRegion texture, int posX, int posY, int width, int height) {
        rendermanager.rawDraw(texture, getAbsoluteX() + posX, getAbsoluteY() + posY, width, height);
    }

    public void drawTextureRelativeSize(TextureRegion texture, int posX, int posY, float width, float height) {
        rendermanager.rawDraw(texture, getAbsoluteX() + posX, getAbsoluteY() + posY, Math.round(getWidth() * width), Math.round(getHeight() * height));
    }

    public void drawTextureRelativeWithin(TextureRegion texture, float posX, float posY, float width, float height) {
        rendermanager.rawDraw(texture, getRelativePosX(posX), getRelativePosY(posY), Math.round(getWidth() * width), Math.round(getHeight() * height));
    }

    public void drawFontText(BitmapFont font, int posX, int posY, String text) {
        font.draw(rendermanager.spriteBatch, text, (int ) (getAbsoluteX() + posX), (int) (getAbsoluteY() + posY));
    }

}