package de.zelosfan.framework.InterfaceElements.advanced;

/**
 * Created by Zelos on 16.06.2015.
 */
public class UIElementRelation {
    public final boolean relativePosition; //coordinates relative within owner or absolute
    public final float positionX, positionY;
    public final UIElement uiElement;


    public UIElementRelation(float _posX, float _posY, boolean _relativePos, UIElement _uiElement) {
        relativePosition = _relativePos;
        positionX = _posX;
        positionY = _posY;
        uiElement = _uiElement;
    }


    //pos: draw position relative to interior of parent
    //width/height: respective parent values
    public void render(int posX, int posY, int width, int height) {
        if (relativePosition) {
            uiElement.render(Math.round(posX + positionX * width), Math.round(positionY + posY * height));
        } else {
            uiElement.render((int) positionX + posX,(int) positionY + posY);
        }
    }
}