package de.zelosfan.framework.InterfaceElements;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Zelos on 31.05.2015.
 */
public abstract class RectangleElement extends Element {
    protected int posX, posY, sizeX, sizeY;

    public RectangleElement(TextureRegion _normalImage, TextureRegion _hoverImage, TextureRegion _clickedImage, int _posX, int _posY, int _sizeX, int _sizeY) {
        super(_normalImage, _hoverImage, _clickedImage);

        posX = _posX;
        posY = _posY;
        sizeX = _sizeX;
        sizeY = _sizeY;
    }

    @Override
    public int getX() {
        return posX;
    }

    @Override
    public int getY() {
        return posY;
    }

    @Override
    public boolean positionInArea(int mX, int mY) {
        int spanX = mX - getX();
        boolean inX = spanX >= 0 && spanX <= sizeX;

        int spanY = mY - getY();
        boolean inY = spanY >= 0 && spanY <= sizeY;

        return inX && inY;
    }
}
