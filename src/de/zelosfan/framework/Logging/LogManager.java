package de.zelosfan.framework.Logging;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.05.13
 * Time: 14:06
 */
public class LogManager {

    private static ObjectMap<Class, Integer> logLevels = new ObjectMap<>();
    public static int DEFAULT_LOGLEVEL = Application.LOG_ERROR;
    public static boolean active = true;

    public static void register(Class logclass, int logLevel) {
        logLevels.put(logclass, logLevel);
    }

    public static synchronized boolean log(Class sender, int logLevel, String message) {
        if (!active) return false;
        if (logLevels.containsKey(sender)) {
            if (logLevels.get(sender) >= logLevel) {
                switch (logLevel) {
                    case Application.LOG_DEBUG:
                        Gdx.app.debug("[DEBUG]" + sender.getSimpleName(), message);
                        return true;
                    case Application.LOG_ERROR:
                        Gdx.app.error("[ERROR]" + sender.getSimpleName(), message);
                        return true;
                    case Application.LOG_INFO:
                        Gdx.app.log("[INFO]" + sender.getSimpleName(), message);
                        return true;
                    case Application.LOG_NONE:
                        return false;
                    default:
                        Gdx.app.debug("[UNKNOWN + " + logLevel + "]" + sender.getSimpleName(), message);
                        return true;
                }
            }
        } else if (DEFAULT_LOGLEVEL >= logLevel) {
            switch (logLevel) {
                case Application.LOG_DEBUG:
                    Gdx.app.debug("[DEBUG]" + sender.getSimpleName(), message);
                    return true;
                case Application.LOG_ERROR:
                    Gdx.app.error("[ERROR]" + sender.getSimpleName(), message);
                    return true;
                case Application.LOG_INFO:
                    Gdx.app.log("[INFO]" + sender.getSimpleName(), message);
                    return true;
                case Application.LOG_NONE:
                    return false;
                default:
                    Gdx.app.debug("[UNKNOWN + " + logLevel + "]" + sender.getSimpleName(), message);
                    return true;
            }
        }
        return false;
    }

}
