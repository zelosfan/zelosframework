package de.zelosfan.framework.Interpolation;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.06.13
 * Time: 16:50
 */
public class TweenHandler {
    ObjectMap<String, TweenObject> tweenMap;

    public TweenHandler() {
        tweenMap = new ObjectMap<>();
    }

    //tick procedure for progression calculation
    public void tick() {
        for (TweenObject tweenObject : tweenMap.values()) {
            tweenObject.tick();
        }
    }

    //add TweenObject to tweenMap, replace old if already mapped
    public void add(String name, TweenObject tweenObject) {
        if (tweenMap.containsKey(name)) {
            tweenMap.remove(name);
        }
        tweenMap.put(name, tweenObject);
    }

    //adds a new tweenValue to tho TweenObject, replaces if already exits
    public TweenObject add(String name, float current, float destination, int duration, Interpolation interpolation) {
        if (tweenMap.containsKey(name)) {
            tweenMap.remove(name);
        }
        tweenMap.put(name, new TweenObject(current, destination, duration, interpolation));
        return tweenMap.get(name);
    }

    //get current value of named TweenObject, returns -1 if not existing
    public float get(String name) {
        if (!tweenMap.containsKey(name)) return -1;
        return tweenMap.get(name).getCurrent();
    }

    //get named TweenObject, returns NULL if not existing
    public TweenObject getObj(String name) {
        if (!tweenMap.containsKey(name)) {
            return null;
        }
        return tweenMap.get(name);
    }

    //return true if namedObj exists
    public boolean exists(String name) {
        return getObj(name) != null;
    }

    //remove named TweenObject, return true if found and removed
    public boolean remove(String name) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.remove(name);
        return true;
    }

    public void removeAll() {
        tweenMap.clear();
    }

    //------------------------------------------------

    //adds the next destination, duration and interpolation to the queue of named TweenObject
    public TweenObject addToQueue(String name, float destination, int duration, Interpolation interpolation) {
        if (!tweenMap.containsKey(name)) {
            return null;
        }

        return tweenMap.get(name).add(destination, duration, interpolation);
    }

    public boolean setInterpolation(String name, Interpolation interpolation) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.get(name).interpolation = interpolation;
        return true;
    }

    public boolean setDestination(String name, float destination) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.get(name).destination = destination;
        return true;
    }

    public boolean addDestination(String name, float destination) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.get(name).destination += destination;
        return true;
    }


    public boolean setDuration(String name, int duration) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.get(name).duration = duration;
        return true;
    }

    public boolean addDuration(String name, int duration) {
        if (!tweenMap.containsKey(name)) {
            return false;
        }
        tweenMap.get(name).duration += duration;
        return true;
    }

}
