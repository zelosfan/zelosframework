package de.zelosfan.framework.Interpolation;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Interpolation;

import java.util.LinkedList;
import java.util.Queue;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.06.13
 * Time: 16:59
 */
public class TweenObject {
    protected float start; //"current"-value at creation of TweenObject; do not fuck with this

    protected float current; //current interpolated value
    protected float destination; //current destination being interpolated to

    protected int progress; //time in tick how often the object ticked
    protected int duration; //total time needed for interpolation of "start" to "destination"
    protected Interpolation interpolation; //Interpolation mode used

    protected Animation.PlayMode playMode; //mode used to rebuild the chain after finish

    protected Queue<TweenPair> chain; //current chain being processed to interpolate
    protected LinkedList<TweenPair> raw; //List used to rebuild chain if it is empty/ completly processed

    private int playTimes = 1; //how often the TweenObject has already been rebuild

    public TweenObject(float _current, float _destination, int _duration, Interpolation _interpolation) {
        current = _current;
        start = _current;

        destination = _destination;
        duration = _duration;
        progress = 0;
        interpolation = _interpolation;

        chain = new LinkedList<>();
        raw = new LinkedList<>();

        TweenPair tp = new TweenPair(destination, duration, interpolation);
        tp.start = start;
        raw.add(tp);

        playMode = Animation.PlayMode.NORMAL;
    }

    public TweenObject(float _current, float _destination, int _duration, Interpolation _interpolation, Animation.PlayMode _loopingMode) {
        this(_current, _destination, _duration, _interpolation);

        playMode = _loopingMode;

        if (playMode.equals(Animation.PlayMode.REVERSED)) {

        }
    }

    //adds TweenObject to rawList and current Queue
    public TweenObject add(float destination, int duration, Interpolation interpolation) {
        TweenPair tp = new TweenPair(destination, duration, interpolation);
        raw.add(tp);
        chain.add(tp);
        return this;
    }

    //adds TweenObject to rawList and then reset the object to build chain
    public TweenObject addToRawListAndReset(float destination, int duration, Interpolation interpolation) {
        TweenPair tp = new TweenPair(destination, duration, interpolation);
        raw.add(tp);

        resetTweenObject();
        return this;
    }

    //reset TweenObject to start values
    public void resetTweenObject() {
        playTimes = 1;
        rebuildChain();
    }

    public float getCurrent() {
        return current;
    }

    public float getDestination() {
        return destination;
    }

    public float getStart() {
        return start;
    }

    public int getProgress() {
        return progress;
    }

    public int getDuration() {
        return duration;
    }

    public Interpolation getInterpolation() {
        return interpolation;
    }

    public Animation.PlayMode getPlayMode() {
        return playMode;
    }

    public void tick() {
        if (progress <= duration) {
            progress++;

            current = interpolation.apply(start, destination, (float) progress / duration);
        } else {
            current = destination;

            if (!chain.isEmpty()) {
                pollNextFromChain();
            } else {
                if (playMode != null) {
                    rebuildChain();
                    pollNextFromChain();
                }
            }
        }
    }

    public void changePlayMode(Animation.PlayMode newPlaymode) {
        playMode = newPlaymode;
        resetTweenObject();
    }

    //rebuild current chain from rawList, reset progress
    private void rebuildChain() {
        if (playMode == null) return;
        playTimes++;

        chain.clear();

        switch (playMode) {
            case NORMAL:
                if (playTimes == 1) {
                    for (TweenPair tweenPair: raw) {
                        chain.add(tweenPair);
                    }
                    start = raw.getFirst().start;
                    current = start;
                    progress = 0;
                }
                break;

            case LOOP:
                for (TweenPair tweenPair: raw) {
                    chain.add(tweenPair);
                }
                start = raw.getFirst().start;
                current = start;
                progress = 0;
                break;

            case LOOP_PINGPONG:
                if (playTimes % 2 == 0) {
                    for (int i = raw.size() - 1; i >= 0; i--) {
                        chain.add(raw.get(i).reversedClone());
                    }
                    start = raw.getLast().destination;
                } else {
                    for (TweenPair tweenPair : raw) {
                        chain.add(tweenPair);
                    }
                    start = raw.getFirst().start;
                }

                current = start;
                progress = 0;
                break;

            case LOOP_REVERSED:
                for (int i = raw.size() - 1; i >= 0; i--) {
                    chain.add(raw.get(i).reversedClone());
                }
                start = raw.getLast().destination;
                current = start;
                progress = 0;
                break;

            case REVERSED:
                if (playTimes == 1) {
                    for (int i = raw.size() - 1; i >= 0; i--) {
                        chain.add(raw.get(i).reversedClone());
                    }
                    start = raw.getLast().destination;
                    current = start;
                    progress = 0;
                }
                break;
        }
    }


    private void pollNextFromChain() {
        if (chain.isEmpty()) return;

        TweenPair tweenPair =  chain.poll();
        destination = tweenPair.destination;
        duration = tweenPair.duration;
        interpolation = tweenPair.interpolation;
        start = current;
        tweenPair.start = start;
        progress = 0;
    }

}
