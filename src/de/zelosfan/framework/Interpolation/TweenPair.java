package de.zelosfan.framework.Interpolation;

import com.badlogic.gdx.math.Interpolation;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.07.13
 * Time: 07:54
 */
public class TweenPair {
    public final float destination;
    public final int duration;
    public final Interpolation interpolation;
    public float start = 0;

    public TweenPair(float _destination, int _duration, Interpolation _interpolation) {
        destination = _destination;
        duration = _duration;
        interpolation = _interpolation;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        TweenPair tweenPair = new TweenPair(destination, duration, interpolation);
        tweenPair.start = start;
        return tweenPair;
    }

    //creates a reversed version of this tweenPair start is end ; end is start
    public TweenPair reversedClone() {
        TweenPair tweenPair = new TweenPair(start, duration, interpolation);
        tweenPair.start = destination;
        return tweenPair;
    }
}
