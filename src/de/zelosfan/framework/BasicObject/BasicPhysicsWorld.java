package de.zelosfan.framework.BasicObject;

import com.badlogic.gdx.math.Vector2;
import de.zelosfan.framework.Rendering.Rendermanager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.02.14
 * Time: 21:48
 */
public class BasicPhysicsWorld {

    public static int MAX_SUPPORTED_OBJECTS = 40;

    public final float air_resistance;
    public Vector2 gravitation;

    public ArrayList<BasicObject> objects = new ArrayList<>(MAX_SUPPORTED_OBJECTS);

    public BasicPhysicsWorld(float _air_resistance, Vector2 _gravitation) {
        air_resistance = _air_resistance;
        gravitation = _gravitation;
    }

    public void addObject(BasicObject basicObject) {
        if (basicObject == null) return;

        objects.add(basicObject);
    }

    public void removeObject(BasicObject basicObject) {
        if (basicObject == null) return;

        objects.remove(basicObject);
    }

    public void tick() {
        LinkedList<BasicObject> dumpObjects = new LinkedList<>();
        for (BasicObject basicObject: objects) {
            if (basicObject.remove) {
                dumpObjects.add(basicObject);
            } else {
                basicObject.tick();
            }
        }

        for (BasicObject basicObject: dumpObjects) {
            objects.remove(basicObject);
        }

        moveAllObjects();
    }

    public void moveAllObjects() {
        for (BasicObject basicObject: objects) {
            basicObject.setAccelX((basicObject.accelX + gravitation.x) * (1 - air_resistance));
            basicObject.moveX();
            if (doesCollideWithAnything(basicObject)) {
                    basicObject.resetToLastValidPosition();
                    basicObject.speedX = 0;
            } else {
                basicObject.assignAsValidPosition();
                basicObject.speedX = basicObject.speedX * (1 - air_resistance);
            }
            basicObject.setAccelY((basicObject.accelY + gravitation.y) * (1 - air_resistance));
            basicObject.moveY();
             if (doesCollideWithAnything(basicObject)) {
                     basicObject.resetToLastValidPosition();
                     basicObject.speedY = 0;
            } else {
                basicObject.assignAsValidPosition();
                basicObject.speedY = basicObject.speedY * (1 - air_resistance);
            }
        }
    }

    //could be improved by constructing a parted map
    public boolean doesCollideWithAnything(BasicObject basicObject) {
        if (!basicObject.collisionable) return false;

        for (BasicObject otherObject: objects) {
            if (basicObject == otherObject) continue;

            if (basicObject.doesCollideWith(otherObject)) {
                basicObject.onCollideWith(otherObject);
                otherObject.onCollideWith(basicObject);
                if (basicObject.getResolve() == ResolveState.DO_RESOLVE && otherObject.getResolve() == ResolveState.DO_RESOLVE) {
                    return true;
                }
            }
        }

        return false;
    }

    public void renderObjects(Rendermanager rendermanager) {
        for (BasicObject otherObject: objects) {
            otherObject.draw(rendermanager);
        }
    }

//    public void renderObjectsWithZBuffer(Rendermanager rendermanager) {
//        Arrays.(objects, (new Comparator<BasicObject>() {
//            @Override
//            public int compare(BasicObject o1, BasicObject o2) {
//                return (o1.z - o2.z);
//            }
//        }));
//        for (BasicObject otherObject: objects) {
//            otherObject.draw(rendermanager);
//        }
//    }

    public void dispose() {
        objects.clear();
    }

}
