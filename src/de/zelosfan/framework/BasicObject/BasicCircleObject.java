package de.zelosfan.framework.BasicObject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import de.zelosfan.framework.Util.RectangleM;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.02.14
 * Time: 13:16
 */
public class BasicCircleObject extends BasicObject {

    float[] lastValidPosition;

    Circle collisionCircle;

    public BasicCircleObject(boolean _collisionable, BodyState _bodyState, float x, float y, float radius, TextureRegion... textureRegions) {
        super(_collisionable, _bodyState, textureRegions);

        collisionCircle = new Circle(x, y, radius);
        calculateOuterRadius();

        lastValidPosition = new float[2];
        lastValidPosition[0] = x;
        lastValidPosition[1] = y;
    }

    @Override
    protected float calculateOuterRadius() {
        outerRadius = collisionCircle.radius;
        return getOuterRadius();
    }

    public boolean doesRoughlyCollideWith(BasicObject basicObject) {
        if (!collisionable) return false;

        float distX = getCenterPoint()[0] - basicObject.getCenterPoint()[0];
        float distY = getCenterPoint()[1] - basicObject.getCenterPoint()[1];

        float dist = (float) (Math.sqrt(Math.abs(distX) + Math.abs(distY)) +  Math.sqrt(Math.abs(distX) + Math.abs(distY)));

        return (getOuterRadius() + basicObject.getOuterRadius() >= dist);
    }

    @Override
    public boolean doesCollideWith(BasicObject otherObject) {
        if (!(collisionable && otherObject.collisionable)) return false;

        if (!doesRoughlyCollideWith(otherObject)) return false;

        if (otherObject instanceof BasicRectangleObject) {
            if (Intersector.overlaps(collisionCircle, ((BasicRectangleObject) otherObject).collisionRectangle)) {
                onCollideWith(otherObject);
                otherObject.onCollideWith(this);
                return true;
            }
            return false;
        }
        if (otherObject instanceof BasicCircleObject) {
            if (collisionCircle.overlaps(((BasicCircleObject) otherObject).collisionCircle)) {
                onCollideWith(otherObject);
                otherObject.onCollideWith(this);
                return true;
            }
            return false;
        }



        throw new RuntimeException("Unknown BasicCollisionObject in BasicRectangleObject(doesCollideWith)");
    }

    @Override
    public void onCollideWith(BasicObject otherObject) {
        super.onCollideWith(otherObject);

        if (!collisionable) throw new RuntimeException("This object is not supposed to collide (collisionable: false)");
    }

    @Override
    public void resetToLastValidPosition() {
        collisionCircle.setX(lastValidPosition[0]);
        collisionCircle.setY(lastValidPosition[1]);
    }

    @Override
    public void assignAsValidPosition() {
        lastValidPosition[0] = collisionCircle.x;
        lastValidPosition[1] = collisionCircle.y;
    }

    @Override
    public float[] getCenterPoint() {
        return new float[]{collisionCircle.x, collisionCircle.y};
    }

    public float[] getBottemLeftPoint() {
        return new float[]{collisionCircle.x - collisionCircle.radius, collisionCircle.y - collisionCircle.radius};
    }

    public float getRadius() {
        return collisionCircle.radius;
    }

    @Override
    public float getY() {
        return collisionCircle.y;
    }

    @Override
    public float getX() {
        return collisionCircle.x;
    }

    @Override
    public void setY(float y) {
        collisionCircle.y = y;
    }

    @Override
    public void setX(float x) {
        collisionCircle.x = x;
    }

    @Override
    public void addY(float y) {
        setY(getY() + y);
    }

    @Override
    public void addX(float x) {
        setX(getX() + x);
    }
}
