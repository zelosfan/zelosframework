package de.zelosfan.framework.BasicObject;

/**
 * Created by Zelos on 14.06.2015.
 */
public enum ResolveState {
    DO_RESOLVE, DONT_RESOLVE
}
