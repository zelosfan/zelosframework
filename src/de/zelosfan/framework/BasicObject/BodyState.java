package de.zelosfan.framework.BasicObject;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.02.14
 * Time: 23:23
 */
public enum BodyState {
    STATIC, DYNAMIC
}
