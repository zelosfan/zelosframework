package de.zelosfan.framework.BasicObject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import de.zelosfan.framework.Util.RectangleM;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.02.14
 * Time: 13:16
 */
public class BasicRectangleObject extends BasicObject {

    float[] lastValidPosition;

    RectangleM collisionRectangle;

    public BasicRectangleObject(boolean _collisionable, BodyState _bodyState, float x, float y, float width, float height, TextureRegion... textureRegions) {
        super(_collisionable, _bodyState, textureRegions);

        collisionRectangle = new RectangleM(x, y, width, height);
        calculateOuterRadius();

        lastValidPosition = new float[2];
        lastValidPosition[0] = x;
        lastValidPosition[1] = y;
    }

    @Override
    protected float calculateOuterRadius() {
        outerRadius = (float) (Math.sqrt(collisionRectangle.getHeight()) + Math.sqrt(collisionRectangle.getWidth()) * Math.sqrt(collisionRectangle.getHeight()) + Math.sqrt(collisionRectangle.getWidth()));
        return getOuterRadius();
    }

    public boolean doesRoughlyCollideWith(BasicObject basicObject) {
        if (!collisionable) return false;

        float distX = getCenterPoint()[0] - basicObject.getCenterPoint()[0];
        float distY = getCenterPoint()[1] - basicObject.getCenterPoint()[1];

        float dist = (float) (Math.sqrt(Math.abs(distX) + Math.abs(distY)) +  Math.sqrt(Math.abs(distX) + Math.abs(distY)));

        return (getOuterRadius() + basicObject.getOuterRadius() >= dist);
    }

    @Override
    public boolean doesCollideWith(BasicObject otherObject) {
        if (!(collisionable && otherObject.collisionable)) return false;

        if (otherObject instanceof BasicRectangleObject) {
            if (collisionRectangle.overlaps(((BasicRectangleObject) otherObject).collisionRectangle)) {
                onCollideWith(otherObject);
                otherObject.onCollideWith(this);
                return true;
            }
            return false;
        }
        if (otherObject instanceof BasicCircleObject) {
            if (Intersector.overlaps(((BasicCircleObject) otherObject).collisionCircle, collisionRectangle)) {
                onCollideWith(otherObject);
                otherObject.onCollideWith(this);
                return true;
            }
            return false;
        }

        if (!doesRoughlyCollideWith(otherObject)) return false;

        throw new RuntimeException("Unknown BasicCollisionObject in BasicRectangleObject(doesCollideWith)");
    }

    @Override
    public void onCollideWith(BasicObject otherObject) {
        super.onCollideWith(otherObject);

        if (!collisionable) throw new RuntimeException("This object is not supposed to collide (collisionable: false)");
    }

    @Override
    public void resetToLastValidPosition() {
        collisionRectangle.setX(lastValidPosition[0]);
        collisionRectangle.setY(lastValidPosition[1]);
    }

    @Override
    public void assignAsValidPosition() {
        lastValidPosition[0] = collisionRectangle.x;
        lastValidPosition[1] = collisionRectangle.y;
    }

    @Override
    public float[] getCenterPoint() {
        return collisionRectangle.getCenterPoint();
    }

    public float getWidth() {
        return collisionRectangle.width;
    }

    public float getHeight() {
        return collisionRectangle.height;
    }

    @Override
    public float getY() {
        return collisionRectangle.getY();
    }

    @Override
    public float getX() {
        return collisionRectangle.getX();
    }

    @Override
    public void setY(float y) {
        collisionRectangle.setY(y);
    }

    @Override
    public void setX(float x) {
        collisionRectangle.setX(x);
    }

    @Override
    public void addY(float y) {
        setY(getY() + y);
    }

    @Override
    public void addX(float x) {
        setX(getX() + x);
    }
}
