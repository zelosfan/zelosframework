package de.zelosfan.framework.BasicObject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Collision.CollideEvent;
import de.zelosfan.framework.Rendering.Rendermanager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 12.02.14
 * Time: 22:39
 */
public abstract class BasicObject {
    public final BodyState bodyState;

    protected TextureRegion[] textures;
    protected float accelX, accelY, speedX, speedY, frictionX, frictionY;

    float outerRadius;
    public float r, g, b, a, rotation;

    protected int z = 1;

    public boolean remove = false;
    protected boolean collisionable; //if false will no be tested for any collision (ignored)

    protected ResolveState resolveState = ResolveState.DO_RESOLVE;

    public CollideEvent userdata = null;

    public BasicObject(boolean _collisionable, BodyState _bodyState, TextureRegion... textureRegions) {
        this(1, _collisionable, _bodyState, textureRegions);
    }

    public BasicObject(int _z, boolean _collisionable, BodyState _bodyState, TextureRegion... textureRegions) {
        collisionable = _collisionable;
        textures = textureRegions;

        bodyState = _bodyState;

        r = 1;
        g = 1;
        b = 1;
        a = 1;

        z = _z;

        rotation = 0;

        accelX = 0;
        accelY = 0;
        speedX = 0;
        speedY = 0;

        frictionX = 1;
        frictionY = 1;
    }

    public void setResolve(ResolveState newResolvestate) {
        resolveState = newResolvestate;
    }

    public ResolveState getResolve() {
        return resolveState;
    }

    public void setZ(int _z) {
        z = _z;
    }

    public void setCollisionable(boolean _collisionable) {
        collisionable = _collisionable;
    }

    public boolean canCollide() {
        return collisionable;
    }

    protected void moveX() {
        if (bodyState == BodyState.STATIC) return;

        speedX += accelX;

        accelX = 0;

        speedX = speedX / frictionX;

        addX(speedX);

        frictionX = 1;
    }

    protected void moveY() {
        if (bodyState == BodyState.STATIC) return;

        speedY += accelY;

        accelY = 0;

        speedY = speedY / frictionY;

        addY(speedY);

        frictionY = 1;
    }


    public float getOuterRadius() {
        return outerRadius;
    }

    public void tick() {

    }

    public void setAccelX(float _accelX) {
        accelX = _accelX;
    }

    public void setAccelY(float _accelY) {
        accelY = _accelY;
    }

    public void addLimitedAccelerationX(float _accelX, float _limitX) {
        if (_accelX > 0) {
            accelX = Math.min(_accelX, _limitX);
        } else {
            accelX = Math.max(_accelX, -_limitX);
        }
    }

    public void addLimitedAccelerationY(float _accelY, float _limitY) {
        if (_accelY > 0) {
            accelY = Math.min(_accelY, _limitY);
        } else {
            accelY = Math.max(_accelY, -_limitY);
        }
    }

    public void setToConstantSpeedX(float constantSpeedX) {
        accelX = constantSpeedX - speedX;
    }

    public void setToConstantSpeedY(float constantSpeedY) {
        accelY = constantSpeedY - speedY;
    }

    public abstract float getX();

    public abstract float getY();

    public abstract void addX(float x);

    public abstract void addY(float y);

    public abstract void setX(float x);

    public abstract void setY(float y);

    public TextureRegion getTexture() {
        return null;
    }

    public abstract void resetToLastValidPosition();

    public abstract void assignAsValidPosition();

    protected abstract float calculateOuterRadius();

    public abstract boolean doesCollideWith(BasicObject otherObject);

    public void onCollideWith(BasicObject otherObject) {
        if (this.userdata != null && otherObject.userdata != null) {
            userdata.onCollide(otherObject.userdata);
        }
    }

    public abstract float[] getCenterPoint();

    public  void draw(Rendermanager rendermanager) {

    }

}
