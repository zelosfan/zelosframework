package de.zelosfan.framework.Localization;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.SerializationException;
import com.badlogic.gdx.utils.XmlReader;
import de.zelosfan.framework.Logging.LogManager;

import java.io.IOException;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 17.05.13
 * Time: 19:35
 */
public class LanguageManager {

    static String language;

    static ObjectMap<String, String> languageMap;

    public static void loadLanguage(String lang) {
        language = lang;

        XmlReader xmlReader = new XmlReader();
        try {
            if (language.toLowerCase().equals("en")) {
                lang = "";
            } else {
                lang = "-" + lang;
            }
            XmlReader.Element element = xmlReader.parse(Gdx.files.internal("res/values" + lang + "/strings.xml"));
            languageMap = new ObjectMap<>();

            for (int i = 0; i < element.getChildCount(); i++) {
                languageMap.put(element.getChild(i).getAttributes().values().next(), element.getChild(i).getText());
            }
        } catch (IOException | SerializationException exception) {
            LogManager.log(LanguageManager.class, Application.LOG_ERROR, "(loadLanguage) Couldn't load language file at " + "res/values" + lang + "/strings.xml" + "# fallback to default");
            loadLanguage("en");
        }

    }


    public static String getString(String name) {
        if (!languageMap.containsKey(name)) {
            LogManager.log(LanguageManager.class, Application.LOG_ERROR, "Missing translation for \"" + name + "\" in Language " + language);
            return name;
        }
        return languageMap.get(name);
    }

}
