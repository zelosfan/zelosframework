package de.zelosfan.framework.Util;

import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

/**
 * Created by Zelos on 15.04.2015.
 */
public class Message implements Pool.Poolable{

    public String event;
    public Object[] objects;

    public Message(String event, Object... objects) {
        this.event = event;
        this.objects = objects;
    }

    public Message set(String event, Object... objects) {
        this.event = event;
        this.objects = objects;
        return this;
    }

    @Override
    public void reset() {
        objects = null;
    }
}
