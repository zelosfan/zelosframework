package de.zelosfan.framework.Util;

import com.badlogic.gdx.math.Vector2;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.05.13
 * Time: 17:31
 */
public class RectangleM extends com.badlogic.gdx.math.Rectangle {
    public RectangleM(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public RectangleM() {
        super();
    }

    public RectangleM(com.badlogic.gdx.math.Rectangle rect) {
        super(rect);
    }

    @Override
    public RectangleM set(float x, float y, float width, float height) {
       super.set(x, y, width, height);
        return this;
    }

    @Override
    public float getX() {
        return super.getX();
    }

    @Override
    public float getY() {
        return super.getY();
    }

    @Override
    public float getWidth() {
        return super.getWidth();
    }

    @Override
    public float getHeight() {
        return super.getHeight();
    }

    @Override
    public RectangleM setPosition(Vector2 position) {
        super.setPosition(position);
        return this;
    }

    @Override
    public RectangleM setPosition(float x, float y) {
        super.setPosition(x, y);
        return this;
    }

    @Override
    public boolean contains(float x, float y) {
        return super.contains(x, y);
    }

    public float[] getCenterPoint() {
        return new float[]{this.x + this.width / 2, this.y + this.height / 2};
    }
}
