package de.zelosfan.framework.Util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.IO.IOManager;
import de.zelosfan.framework.IO.ResourceLoader;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.CameraManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Rendering.Rendermode;
import de.zelosfan.framework.Service.ServiceLocator;
import de.zelosfan.framework.Timing.GametimeManager;

import javax.swing.*;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 30.03.2015
 * Time: 17:25
 */
public class TestEnvironment extends ApplicationAdapter {

    public int TPS = 100;
    public int FPS = 60;

    //RESOURCEFOLDERS
    public String MUSICPATH = "music/";
    public String SOUNDPATH = "sfx/";
    public String SHADERPATH = "shader/";
    public String FONT_PACKPATH = "font.atlas";
    public String TEX_PACKPATH = "textures.atlas";
    //--------------------------

    public ObjectMap<String, TextureRegion> textures = new ObjectMap<>();
    public ObjectMap<String, Sound> sfxMap = new ObjectMap<>();
    public ObjectMap<String, ObjectMap<Integer, BitmapFont>> fontMap = new ObjectMap<>();
    public ObjectMap<String, ShaderProgram> shaderProgramObjectMap = new ObjectMap<>();

    public ResourceLoader resourceLoader;
    public Rendermanager rendermanager;
    public CameraManager cameraManager;

    @Override
    public void create() {
        super.create();

        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        LogManager.DEFAULT_LOGLEVEL = Application.LOG_DEBUG;
    }

    public void completeInit() {
        loadAssets();
        initialize();
    }

    public void loadAssets() {
        resourceLoader = new ResourceLoader();
        resourceLoader.loadImagePackIntoTextureRegions(TEX_PACKPATH, textures);
        resourceLoader.loadFontSizes(FONT_PACKPATH, fontMap);
        resourceLoader.loadDirectoryFiles(SOUNDPATH, sfxMap, Sound.class, true);

        ResourceLoader.loadShaders(SHADERPATH, shaderProgramObjectMap);
    }

    public void initialize() {
        ServiceLocator.gametimeHandler.setTPS(TPS);
        ServiceLocator.ioManager.initialize();
        rendermanager = new Rendermanager(Rendermode.PERSPECTIVE, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), shaderProgramObjectMap);
    }


    public void tick() {
        GameStateManager.getGameStateManager().tick();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        ServiceLocator.gametimeHandler.updateDeltaTime(Gdx.graphics.getDeltaTime());
        while (ServiceLocator.gametimeHandler.tick()) {
            tick();
        }

        rendermanager.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
        resourceLoader.dispose();
        rendermanager.dispose();
    }
}
