package de.zelosfan.framework.Util;

import com.badlogic.gdx.utils.ObjectMap;

import java.util.Random;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 22.05.13
 * Time: 05:12
 */
public class RandomUtil {
    public final static ObjectMap<String, Long> identifiers = new ObjectMap<>(); //Make sure to update everytime one changes

    public static Random getNanoRandom() {
        return new Random(System.nanoTime());
    }

    public static Random getIdRandom(String identifier, Integer nr) {
        if (!identifiers.containsKey(identifier)) updateID(identifier);
        nr *= 123;
        nr += 123456;
        return new Random(identifiers.get(identifier) + nr);
    }

    public static Long getID(String identifier) {
        return identifiers.get(identifier);
    }

    public static void updateID(String identifier) {
        updateID(identifier, System.nanoTime());
    }

    public static void updateID(String identifier, Long newID) {
        identifiers.put(identifier, newID);
    }
}
