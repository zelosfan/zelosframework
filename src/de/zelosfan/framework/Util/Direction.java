package de.zelosfan.framework.Util;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 14.01.14
 * Time: 08:00
 */
public enum Direction {
    NORTH(0), WEST(-270), SOUTH(-180), EAST(-90);

    public final int rotdeg;

    Direction(int deg) {
        rotdeg = deg;
    }


}
