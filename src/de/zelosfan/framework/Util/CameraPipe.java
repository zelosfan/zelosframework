package de.zelosfan.framework.Util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import de.zelosfan.framework.Interpolation.TweenHandler;
import de.zelosfan.framework.Interpolation.TweenObject;

import java.util.Vector;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 13.01.14
 * Time: 09:47
 */
public class CameraPipe {

    private static Camera camera;
    private static TweenHandler tweenHandler = new TweenHandler();
    private static TweenHandler lookatHandler = new TweenHandler();

    public static int scrollDur = 24;
    public static float scrollDist = 4;

    public static boolean useLookAtHandler = false;

    private static float screenShakeIntensity = 0;
    private static int screenShakeDuration = 0;
    private static int screenShakeSpaceTime = 0;
    private static int screenShakeSpaceTimeCountdown = 0;
    private static Vector2 screenShakeOffset = new Vector2(0, 0);

    private static float dirX, dirY;

    public static void setCamera(Camera _camera) {
        camera = _camera;
        tweenHandler.removeAll();
        tweenHandler.add("X", camera.position.x, camera.position.x, 0, Interpolation.sine);
        tweenHandler.add("Y", camera.position.y, camera.position.y, 0, Interpolation.sine);
        tweenHandler.add("Z", camera.position.z, camera.position.z, 0, Interpolation.sine);

        if (useLookAtHandler) {
        }
    }

    public static float getMovementX() {
        return dirX;
    }

    public static float getMovementY() {
        return dirY;
    }

    public static void setMovementX(float x) {
        dirX = x;
    }

    public static void setMovementY(float y) {
        dirY = y;
    }

    public static void moveInDirX(float x) {
        dirX += x;
    }

    public static void moveInDirY(float y) {
        dirY += y;
    }

    public static void focusOnPosition(float x, float y, float z) {
        TweenObject temp = tweenHandler.getObj("X");
        tweenHandler.add("X", temp.getCurrent(), x, scrollDur, Interpolation.sineOut);

        temp = tweenHandler.getObj("Y");
        tweenHandler.add("Y", temp.getCurrent(), y, scrollDur, Interpolation.sineOut);

        temp = tweenHandler.getObj("Z");
        tweenHandler.add("Z", temp.getCurrent(), z, scrollDur, Interpolation.sineOut);
    }

    public static void screenShake(float _screenShakeIntensity, int _screenShakeSpaceTime, int _screenShakeDuration) {
        screenShakeIntensity = _screenShakeIntensity;
        screenShakeDuration = _screenShakeDuration;
        screenShakeSpaceTime = _screenShakeSpaceTime;
    }

    public static void render() {
        render(null);
    }

    public static void render(RectangleM borderRestriction) {
        if (screenShakeDuration > 0) {
            screenShakeDuration--;

            if (screenShakeSpaceTimeCountdown <= 0) {
                screenShakeOffset.x = MathUtils.random(-screenShakeIntensity, screenShakeIntensity);
                screenShakeOffset.y = MathUtils.random(-screenShakeIntensity, screenShakeIntensity);

                screenShakeSpaceTimeCountdown = screenShakeSpaceTime;
            } else {
                screenShakeSpaceTimeCountdown--;
            }
        }


        if (dirX > 0) {
            TweenObject temp = tweenHandler.getObj("X");
            tweenHandler.add("X", temp.getCurrent(), temp.getDestination() + scrollDist * dirX, scrollDur, Interpolation.sineOut);
        }
        if (dirX < 0) {
            TweenObject temp = tweenHandler.getObj("X");
            tweenHandler.add("X", temp.getCurrent(), temp.getDestination() + scrollDist * dirX, scrollDur, Interpolation.sineOut);
        }
        if (dirY > 0) {
            TweenObject temp = tweenHandler.getObj("Y");
            tweenHandler.add("Y", temp.getCurrent(), temp.getDestination() + scrollDist * dirY, scrollDur, Interpolation.sineOut);
        }
        if (dirY < 0) {
            TweenObject temp = tweenHandler.getObj("Y");
            tweenHandler.add("Y", temp.getCurrent(), temp.getDestination() + scrollDist * dirY, scrollDur, Interpolation.sineOut);
        }

        tweenHandler.tick();


        if (borderRestriction != null) {
            TweenObject tweenObjectX = tweenHandler.getObj("X");
            float correctedX = getCorrectedCameraX(camera, tweenHandler.get("X"), borderRestriction.getX(), borderRestriction.getWidth());
            float correctedDest = getCorrectedCameraX(camera, tweenObjectX.getDestination(), borderRestriction.getX(), borderRestriction.getWidth());
            tweenHandler.add("X", correctedX, correctedDest, tweenObjectX.getDuration(), tweenObjectX.getInterpolation());

            TweenObject tweenObjectY = tweenHandler.getObj("Y");
            float correctedY = getCorrectedCameraY(camera, tweenHandler.get("Y"), borderRestriction.getY(), borderRestriction.getHeight());
            float correctedDestY = getCorrectedCameraY(camera, tweenObjectY.getDestination(), borderRestriction.getY(), borderRestriction.getHeight());
            tweenHandler.add("Y", correctedY, correctedDestY, tweenObjectY.getDuration(), tweenObjectY.getInterpolation());
        }



        camera.position.x = tweenHandler.get("X");
        camera.position.y = tweenHandler.get("Y");

        if (screenShakeDuration > 0) {
            camera.position.x += screenShakeOffset.x;
            camera.position.y += screenShakeOffset.y;
        }


        camera.position.z = tweenHandler.get("Z");
    }

    public static float getCorrectedCameraY(Camera camera, float newY, float lowerBorder, float higherBorder) {
        if (newY - camera.viewportHeight / 2 < lowerBorder) return lowerBorder + camera.viewportHeight / 2;
        if (newY + camera.viewportHeight / 2 > higherBorder) {
            return higherBorder - camera.viewportHeight / 2;
        }

        return newY;
    }


    public static float getCorrectedCameraX(Camera camera, float newX, float lowerBorder, float higherBorder) {
        if (newX - camera.viewportWidth / 2 < lowerBorder) return lowerBorder + camera.viewportWidth / 2;
        if (newX + camera.viewportWidth / 2 > higherBorder) {
            return higherBorder - camera.viewportWidth / 2;
        }

        return newX;
    }

    public static int increaseDstToCenterX(float screenX, float width) {
        float dX = screenX - width / 2;

        return (int) (screenX + dX * Math.PI / 4);
    }

    public static int increaseDstToCenterY(float screenY, float height) {
        float dY = screenY - height / 2;

        return (int) (screenY + dY * Math.PI / 4);
    }

    public static int getWorldMousePosX(float screenPos, Camera camera) {
        //
        if (camera instanceof OrthographicCamera) {
            return (int) (screenPos + camera.position.x - camera.viewportWidth / 2);
        } else {
           // return (int) camera.unproject(new Vector3(screenPos, 0, 0)).x;
           return (int) (screenPos + camera.position.x - camera.viewportWidth / 2);
        }
    }

    public static Vector3 getWorldMousePosX(float screenPosX, float screenPosY, Camera camera) {
            return camera.unproject(new Vector3(screenPosX, screenPosY, 1));
    }

    public static int getWorldMousePosY(float screenPos, Camera camera) {
      //  return (int) camera.unproject(new Vector3(screenPos, 0, 0)).y;
        if (camera instanceof OrthographicCamera) {
            return (int) (screenPos + camera.position.y - camera.viewportHeight / 2);
        } else {
            return (int) (screenPos + camera.position.y - camera.viewportHeight / 2);
        }
    }
}
