package de.zelosfan.framework.Util;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.List;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 20.05.13
 * Time: 06:34
 */
public class MathUtil {

    //////Box2D force stuff///////////////////////////////
    public static float getGradualImpulseForce(float desiredMaxSpeed, float velocityStep, float currentSpeed, float mass) {
        float desiredSpeed;
        if (velocityStep < 0) {
            desiredSpeed = Math.max(currentSpeed + velocityStep, desiredMaxSpeed);
        } else {
            desiredSpeed = Math.min(currentSpeed + velocityStep, desiredMaxSpeed);
        }
        return getImpulseForceForSpeed(desiredSpeed, currentSpeed, mass);
    }

 /*   public static float getImpulseForceForSpeedX(float desiredSpeed, GameObjectOld gameObject) {
        return getImpulseForceForSpeed(desiredSpeed, gameObject.getBody().getLinearVelocity().x, gameObject.getBody().getMass());
    }

    public static float getImpulseForceForSpeedY(float desiredSpeed, GameObjectOld gameObject) {
        return getImpulseForceForSpeed(desiredSpeed, gameObject.getBody().getLinearVelocity().y, gameObject.getBody().getMass());
    } */

    public static float getImpulseForceForSpeed(float desiredSpeed, float currentSpeed, float mass) {
        return (desiredSpeed - currentSpeed) * mass;
    }
    //-------------------------------------------------------

    public static float mapMathRange(float x, float a1, float a2, float b1, float b2) {
        if ((a1 == 0 && a2 == 0) || (b1 == 0 && b2 == 0) || (x < a1 || x > a2)) {
            return 0;
        }
        return (x - a1) / (a2 - a1) * (b2 - b1) + b1;
    }

    //Greatest Common Divider
    public static int GCD(int a, int b) {
        if (b == 0) return a;
        return GCD(b, a % b);
    }

    public static float getAngle(float x1, float y1, float x2, float y2) {
        float angle = (float) Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));

        if(angle < 0){
            angle += 360;
        }

        return angle;
    }

    public static double round(double unrounded, int precision, RoundingMode roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    //somebody told this would be the same as the next function idk
    public static float getNormalizedVector(float v1, float v2) {
        return (v1 + v2) * 0.707f; //sin(PI/4)
    }

    //this had a function some time ago?
    public static float getFixedWayLength(float main, float second, float applied) {
        if (main == 0) {
            return 0;
        }

        if (second == 0) {
            return applied;
        }

        double c = Math.sqrt(main * main + second * second);
        if (c != 0 && !Double.isInfinite(c) && !Double.isNaN(c)) {
            return (float) Math.abs(main * applied / c);
        }
        return 0;
    }

    //this may or may not work lol
    public static Pixmap rotatePixmap(Pixmap src, float angle){
        final int width = src.getWidth();
        final int height = src.getHeight();
        Pixmap rotated = new Pixmap(width, height, src.getFormat());

        final double radians = Math.toRadians(angle), cos = Math.cos(radians), sin = Math.sin(radians);


        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                final int
                        centerx = width/2, centery = height / 2,
                        m = x - centerx,
                        n = y - centery,
                        j = ((int) (m * cos + n * sin)) + centerx,
                        k = ((int) (n * cos - m * sin)) + centery;
                if (j >= 0 && j < width && k >= 0 && k < height){
                    rotated.drawPixel(x, y, src.getPixel(k, j));
                }
            }
        }
        return rotated;

    }

    public static float getRotationSwappedX(Vector2 dir) {
        if (dir == null) {
            return 0;
        }
        float rotation = 0;

        if (dir.x == -1) {
            rotation += 90;
        } else if (dir.x == 1) {
            rotation += 270;
        }

        if (dir.y == 1) {
            if (rotation > 180) {
                rotation += 45;
            } else if (rotation != 0) {
                rotation -= 45;
            }
        } else if (dir.y == -1) {
            if (rotation > 180) {
                rotation -= 45;
            } else if (rotation != 0) {
                rotation += 45;
            } else {
                rotation += 180;
            }
        }

        return rotation;
    }

    public static float getDistance(float x1, float y1, float x2, float y2) {
        return (float) (Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }

    public static float getRotation(Vector2 dir) {
        if (dir == null) {
            return 0;
        }
        float rotation = 0;

        if (dir.x == -1) {
            rotation += 270;
        } else if (dir.x == 1) {
            rotation += 90;
        }

        if (dir.y == 1) {
            if (rotation > 180) {
                rotation += 45;
            } else if (rotation != 0) {
                rotation -= 45;
            }
        } else if (dir.y == -1) {
            if (rotation > 180) {
                rotation -= 45;
            } else if (rotation != 0) {
                rotation += 45;
            } else {
                rotation += 180;
            }
        }

        return rotation;
    }

    public static Direction getDirectionFromVector(Vector2 vel) {
        return getDirectionFromAngle(getAngle(0, 0, vel.x, vel.y));
    }

    public static Direction getDirectionFromAngle(float deg) {
        if (deg >= 315 || deg <= 45) {
            return Direction.EAST;
        } else if (deg >= 45 && deg <= 135) {
            return Direction.NORTH;
        } else if (deg >= 135 && deg <= 225) {
            return Direction.WEST;
        } else if (deg >= 225 && deg <= 315) {
            return Direction.SOUTH;
        } else {
            return Direction.NORTH;
        }
    }

    public static float reduceToDirection(float nr) {
        return nr / Math.abs(nr);
    }

    //////////MISC
    public static int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next();
        }
        return ret;
    }

    public static float[] convertFloats(List<Float> floats) {
        float[] ret = new float[floats.size()];
        Iterator<Float> iterator = floats.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next();
        }
        return ret;
    }
    ///-----------------------------------
}
