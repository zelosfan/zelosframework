package de.zelosfan.framework.Util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.ArrayList;

/**
 * Original Source: LibGDX
 * Modified by User: Simon "Zelosfan" Herfert
 * Date: 21.05.13
 * Time: 05:10
 */
public class Animation {
    public final static ObjectMap<String, Animation> animationList = new ObjectMap<>();

    public static final int NORMAL = 0;
    public static final int REVERSED = 1;
    public static final int LOOP = 2;
    public static final int LOOP_REVERSED = 3;
    public static final int LOOP_PINGPONG = 4;
    public static final int LOOP_RANDOM = 5;

    final TextureRegion[] keyFrames;
    public float frameDuration;
    public float animationDuration;
    public float timeElapsed;
    public boolean dirty;

    private int playMode = NORMAL;

    public static Animation get(String name) {
        return animationList.get(name);
    }

//    public static TextureRegion getTexture(TextureRegion[] animationTextures, int playMode, float frameDuration, float timePassed) {

//    }

    public static void loadAnimationDefinitions(ObjectMap<String, TextureRegion> textures, String filepath) {
        JsonValue value = new JsonReader().parse(Gdx.files.internal(filepath)).get("animations");
        for (JsonValue entry = value.child(); entry != null; entry = entry.next()) {

            int playtyp;
            switch (entry.getString("playType")) {
                case "NORMAL":
                    playtyp = Animation.NORMAL;
                    break;
                case "LOOP":
                    playtyp = Animation.LOOP;
                    break;
                case "LOOP_PINGPONG":
                    playtyp = Animation.LOOP_PINGPONG;
                    break;
                case "LOOP_REVERSED":
                    playtyp = Animation.LOOP_REVERSED;
                    break;
                case "LOOP_RANDOM":
                    playtyp = Animation.LOOP_RANDOM;
                    break;
                case "REVERSED":
                    playtyp = Animation.REVERSED;
                    break;
                default:
                    playtyp = Animation.NORMAL;
                    break;
            }

     //       System.out.print(entry.getString(entry.name));
            animationList.put(entry.getString("name"), new Animation(entry.getFloat("frameDuration"), Animation.getAnimationTextures(textures, entry.getString("texturename")), playtyp));
           // animationList.get(entry.getString("texturename")).initialize(entry.getFloat("frameDuration"), playtyp);
        }
    }

    /**
     * Constructor, storing the frame duration and key frames.
     *
     * @param frameDuration the time between frames in seconds.
     * @param keyFrames     the {@link TextureRegion}s representing the frames.
     */
    public Animation(float frameDuration, Array<? extends TextureRegion> keyFrames) {
        this.frameDuration = frameDuration;
        this.animationDuration = keyFrames.size * frameDuration;
        this.keyFrames = new TextureRegion[keyFrames.size];
        for (int i = 0, n = keyFrames.size; i < n; i++) {
            this.keyFrames[i] = keyFrames.get(i);
        }

        this.playMode = NORMAL;
        dirty = false;
    }

    /**
     * Constructor, storing the frame duration, key frames and play type.
     *
     * @param frameDuration the time between frames in seconds.
     * @param keyFrames     the {@link TextureRegion}s representing the frames.
     * @param playType      the type of animation play (NORMAL, REVERSED, LOOP, LOOP_REVERSED, LOOP_PINGPONG, LOOP_RANDOM)
     */
    public Animation(float frameDuration, Array<? extends TextureRegion> keyFrames, int playType) {

        this.frameDuration = frameDuration;
        this.animationDuration = keyFrames.size * frameDuration;
        this.keyFrames = new TextureRegion[keyFrames.size];
        for (int i = 0, n = keyFrames.size; i < n; i++) {
            this.keyFrames[i] = keyFrames.get(i);
        }

        this.playMode = playType;
        dirty = false;
    }

    public Animation(Array<? extends TextureRegion> keyFrames) {
        this.keyFrames = new TextureRegion[keyFrames.size];
        for (int i = 0, n = keyFrames.size; i < n; i++) {
            this.keyFrames[i] = keyFrames.get(i);
        }
        dirty = true;
        playMode = NORMAL;
    }

    /**
     * Constructor, storing the frame duration and key frames.
     *
     * @param frameDuration the time between frames in seconds.
     * @param keyFrames     the {@link TextureRegion}s representing the frames.
     */
    public Animation(float frameDuration, TextureRegion... keyFrames) {
        this.frameDuration = frameDuration;
        this.animationDuration = keyFrames.length * frameDuration;
        this.keyFrames = keyFrames;
        this.playMode = NORMAL;
        dirty = false;
    }

    public Animation(TextureRegion... keyFrames) {
        this.keyFrames = keyFrames;
        playMode = NORMAL;
        dirty = true;
    }

    public static Array<? extends TextureRegion> getAnimationTextures(ObjectMap<String, TextureRegion> textures, String animationName) {
        Array<TextureRegion> textureRegions = new Array<>();
        int i = 0;
        System.out.println(animationName);
        while (textures.containsKey(animationName + i + "l")) {
            textureRegions.add(textures.get(animationName + i + "l"));
            System.out.println(animationName + i + "l");
            i++;
        }

        return textureRegions;
    }

    public void update(float timeDelta) {
        timeElapsed += timeDelta;
    }

    public TextureRegion getTexture() {
        return getKeyFrame(timeElapsed);
    }

    /**
     * Returns a {@link TextureRegion} based on the so called state time. This is the amount of seconds an object has spent in the
     * state this Animation instance represents, e.g. running, jumping and so on. The mode specifies whether the animation is
     * looping or not.
     *
     * @param stateTime the time spent in the state represented by this animation.
     * @param looping   whether the animation is looping or not.
     * @return the TextureRegion representing the frame of animation for the given state time.
     */
    public TextureRegion getKeyFrame(float stateTime, boolean looping) {
        if (dirty) {
            Gdx.app.error("Animation", "Animation is dirty (only minimal constructors)");
            return null;
        }

        // we set the play mode by overriding the previous mode based on looping
        // parameter value
        if (looping && (playMode == NORMAL || playMode == REVERSED)) {
            if (playMode == NORMAL)
                playMode = LOOP;
            else
                playMode = LOOP_REVERSED;
        } else if (!looping && !(playMode == NORMAL || playMode == REVERSED)) {
            if (playMode == LOOP_REVERSED)
                playMode = REVERSED;
            else
                playMode = LOOP;
        }
        
        return getKeyFrame(stateTime);
    }

    /**
     * Returns a {@link TextureRegion} based on the so called state time. This is the amount of seconds an object has spent in the
     * state this Animation instance represents, e.g. running, jumping and so on using the mode specified by
     * {@link #setPlayMode(int)} method.
     *
     * @param stateTime
     * @return the TextureRegion representing the frame of animation for the given state time.
     */
    public TextureRegion getKeyFrame(float stateTime) {
        int frameNumber = getKeyFrameIndex(stateTime);
        return keyFrames[frameNumber];
    }

    public void initialize(int frameDuration, int playType) {
        setFrameDuration(frameDuration);
        setPlayMode(playType);
    }

    /**
     * Returns the current frame number.
     *
     * @param stateTime
     * @return current frame number
     */
    public int getKeyFrameIndex(float stateTime) {
        int frameNumber = (int) (stateTime / frameDuration);

        if (keyFrames.length == 1)
            return 0;

        switch (playMode) {
            case NORMAL:
                frameNumber = Math.min(keyFrames.length - 1, frameNumber);
                break;
            case LOOP:
                frameNumber = frameNumber % keyFrames.length;
                break;
            case LOOP_PINGPONG:
                frameNumber = frameNumber % ((keyFrames.length * 2) - 2);
                if (frameNumber >= keyFrames.length)
                    frameNumber = keyFrames.length - 2 - (frameNumber - keyFrames.length);
                break;
            case LOOP_RANDOM:
                frameNumber = MathUtils.random(keyFrames.length - 1);
                break;
            case REVERSED:
                frameNumber = Math.max(keyFrames.length - frameNumber - 1, 0);
                break;
            case LOOP_REVERSED:
                frameNumber = frameNumber % keyFrames.length;
                frameNumber = keyFrames.length - frameNumber - 1;
                break;

            default:
                // play normal otherwise
                frameNumber = Math.min(keyFrames.length - 1, frameNumber);
                break;
        }

        return frameNumber;
    }

    /**
     * Sets the animation play mode.
     *
     * @param playMode can be one of the following: Animation.NORMAL, Animation.REVERSED, Animation.LOOP, Animation.LOOP_REVERSED,
     *                 Animation.LOOP_PINGPONG, Animation.LOOP_RANDOM
     */
    public void setPlayMode(int playMode) {
        this.playMode = playMode;
    }

    public void setFrameDuration(int duration) {
        this.frameDuration = duration;
        this.animationDuration = keyFrames.length * frameDuration;
        dirty = false;
    }

    /**
     * Whether the animation would be finished if played without looping (PlayMode Animation#NORMAL), given the state time.
     *
     * @param stateTime
     * @return whether the animation is finished.
     */
    public boolean isAnimationFinished(float stateTime) {
        int frameNumber = (int) (stateTime / frameDuration);
        return keyFrames.length - 1 < frameNumber;
    }
}
