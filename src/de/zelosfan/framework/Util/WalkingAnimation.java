package de.zelosfan.framework.Util;

import com.badlogic.gdx.Gdx;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 25.05.13
 * Time: 11:24
 */
public class WalkingAnimation {
    Animation[] animations;


    public WalkingAnimation(String walkLeft, String walkRight, String walkTop, String walkBottom) {
        animations = new Animation[4];

        if (Animation.animationList.containsKey(walkTop)) {
            animations[0] = Animation.animationList.get(walkTop);
        } else {
            Gdx.app.error("WalkingAnimation", "Couldn't find Animation " + walkTop);
        }
        if (Animation.animationList.containsKey(walkRight)) {
            animations[1] = Animation.animationList.get(walkRight);
        } else {
            Gdx.app.error("WalkingAnimation", "Couldn't find Animation " + walkTop);
        }
        if (Animation.animationList.containsKey(walkBottom)) {
            animations[2] = Animation.animationList.get(walkBottom);
        } else {
            Gdx.app.error("WalkingAnimation", "Couldn't find Animation " + walkTop);
        }
        if (Animation.animationList.containsKey(walkLeft)) {
            animations[3] = Animation.animationList.get(walkLeft);
        } else {
            Gdx.app.error("WalkingAnimation", "Couldn't find Animation " + walkTop);
        }
    }

    public Animation getWalkLeft() {
        return animations[3];
    }

    public Animation getWalkRight() {
        return animations[1];
    }

    public Animation getWalkTop() {
        return animations[0];
    }

    public Animation getWalkBottom() {
        return animations[2];
    }


}

