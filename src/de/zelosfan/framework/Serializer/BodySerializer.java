package de.zelosfan.framework.Serializer;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import de.zelosfan.framework.Physics.PhysicsManager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 05.09.13
 * Time: 11:47
 */
public class BodySerializer extends Serializer<Body> {
    @Override
    public void write(Kryo kryo, Output output, Body body) {
        output.writeFloat(body.getPosition().x);
        output.writeFloat(body.getPosition().y);
        kryo.writeClassAndObject(output, body.getType());

/*        if (body.getFixtureList().size() > 0) {
            output.writeBoolean(true);
            if (body.getFixtureList().get(0).getShape() != null) {
                kryo.writeClassAndObject(output, body.getFixtureList().get(0).getShape());
            } /* else {

                if (body.getUserData() instanceof GameObject) {
                    GameObject object = (GameObject) body.getUserData();

                    float[] shapeVertices = new float[8];
                    shapeVertices[0] = 0;
                    shapeVertices[1] = 0;
                    shapeVertices[2] = 0;
                    shapeVertices[3] = object.sizeY;
                    shapeVertices[4] = object.sizeX;
                    shapeVertices[5] = object.sizeY;
                    shapeVertices[6] = object.sizeX;
                    shapeVertices[7] = 0;

                    PolygonShape shape = new PolygonShape();
                    shape.set(shapeVertices);
                } else {
                    Gdx.app.debug("", "gg");
                }

            }

            output.writeFloat(body.getFixtureList().get(0).getDensity());
            output.writeFloat(body.getFixtureList().get(0).getFriction());
            output.writeFloat(body.getFixtureList().get(0).getRestitution());

            output.writeBoolean(body.getFixtureList().get(0).isSensor());

        } else {
            output.writeBoolean(false);
        }*/
    }

    @Override
    public Body read(Kryo kryo, Input input, Class<Body> bodyClass) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.x = input.readFloat();
        bodyDef.position.y = input.readFloat();
        bodyDef.type = (BodyDef.BodyType) kryo.readClassAndObject(input);

        boolean fitureExists = input.readBoolean();

        Body body = PhysicsManager.world.createBody(bodyDef);

        if (fitureExists) {

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = (Shape) kryo.readClassAndObject(input);
            fixtureDef.density = input.readFloat();
            fixtureDef.friction = input.readFloat();
            fixtureDef.restitution = input.readFloat();
            fixtureDef.isSensor = input.readBoolean();
            body.createFixture(fixtureDef);
        }

        body.setUserData(this);

        return body;
    }
}