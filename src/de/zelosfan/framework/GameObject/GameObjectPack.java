package de.zelosfan.framework.GameObject;

import de.zelosfan.framework.GameObject.Components.GameObjectComponent;
import de.zelosfan.framework.GameObject.Components.GraphicsComponent;
import de.zelosfan.framework.GameObject.Components.PhysicsComponent;

/**
 * Created by Zelos on 15.04.2015.
 */
public class GameObjectPack {

    float positionX;
    float positionY;
    float positionZ;
    float speedX;
    float speedY;
    float speedZ;
    float accelerationX;
    float accelerationY;
    float accelerationZ;
    float rotation;

    GraphicsComponent graphicsComponent;
    PhysicsComponent physicsComponent;
    GameObjectComponent[] components;

    public GameObjectPack(float positionX, float positionY, float positionZ, float speedX, float speedY, float speedZ, float accelerationX, float accelerationY, float accelerationZ, float rotation, GraphicsComponent graphicsComponent, PhysicsComponent physicsComponents, GameObjectComponent[] components) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.positionZ = positionZ;
        this.speedX = speedX;
        this.speedY = speedY;
        this.speedZ = speedZ;
        this.accelerationX = accelerationX;
        this.accelerationY = accelerationY;
        this.accelerationZ = accelerationZ;
        this.rotation = rotation;
        this.graphicsComponent = graphicsComponent;
        this.physicsComponent = physicsComponents;
        this.components = components;
    }
}
