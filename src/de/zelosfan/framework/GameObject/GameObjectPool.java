package de.zelosfan.framework.GameObject;

import com.badlogic.gdx.utils.Pool;

/**
 * Created by Zelos on 15.04.2015.
 */
public abstract class GameObjectPool extends Pool<GameObject> {

    World world;

    public GameObjectPool(World _world) {
        super();

        world = _world;
    }

    @Override
    public GameObject obtain() {
        GameObject object = super.obtain();
        world.gameObjects.add(object);
        return object;
    }

    @Override
    public void free(GameObject object) {
        super.free(object);
        world.gameObjects.remove(object);
    }

    @Override
    protected abstract GameObject newObject();
}
