package de.zelosfan.framework.GameObject;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import de.zelosfan.framework.Util.Message;

import java.util.ArrayList;

/**
 * Created by Zelos on 15.04.2015.
 */
public class World implements Disposable{


    public static float SPEED_FRICTION = 0.1f;
    public static float ACCELERATION_FRICTION = 0.1f;

    public static int INITIAL_GAMEOBJECT_CAPACITY = 100;

    public static Pool<Vector3> vector3Pool;
    public static Pool<GameObject> gameObjectPool;
    public static Pool<Message> gameObjectMessages;

    public ArrayList<GameObject> gameObjects;


    private boolean initialized = false;

    public World() {
        vector3Pool = Pools.get(Vector3.class);
        gameObjectMessages = Pools.get(Message.class);
    }

    public void init(GameObjectPool _gameObjectPool) {
        gameObjectPool = _gameObjectPool;
        initialized = true;
    }

    public void tick() {
        if (!initialized) return;

        while (gameObjects.iterator().hasNext()) {
            GameObject gameObject =  gameObjects.iterator().next();

            if (gameObject.isEnabled()) {
                gameObject.tick();
            }
            if (gameObject.isRemove()) {
                gameObject.setEnabled(false);
                gameObjects.remove(gameObject);
            }
        }

        physicsTick();
    }

    public void physicsTick() {
        while (gameObjects.iterator().hasNext()) {
            GameObject gameObject =  gameObjects.iterator().next();

            if (gameObject.isEnabled()) {
                gameObject.physicsTick();
            }
        }
    }

    @Override
    public void dispose() {
        for (GameObject gameObject: gameObjects) {
            gameObjectPool.free(gameObject);
        }
        gameObjects.clear();
    }
}
