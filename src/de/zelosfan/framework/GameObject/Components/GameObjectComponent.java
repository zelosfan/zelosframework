package de.zelosfan.framework.GameObject.Components;

import com.badlogic.gdx.utils.Disposable;
import de.zelosfan.framework.GameObject.GameObject;

/**
 * Created by Zelos on 15.04.2015.
 */
public abstract class GameObjectComponent implements Disposable {

    protected boolean enabled;

    protected GameObject gameObject;

    public GameObjectComponent(GameObject _gameObject) {
        enabled = true;
        gameObject = _gameObject;
    }

    public void receive(String message, Object... objects) {

    }

    public boolean isEnabled() {
        return enabled;
    }

    public void disable() {
        enabled = false;
    }

    public void enable(){
        enabled = true;
    }

    public void tick() {

    }

    public void render() {

    }

}
