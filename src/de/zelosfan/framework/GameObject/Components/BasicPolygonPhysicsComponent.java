package de.zelosfan.framework.GameObject.Components;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import de.zelosfan.framework.GameObject.GameObject;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Zelos on 21.04.2015.
 */
public class BasicPolygonPhysicsComponent extends BasicPhysicsComponent {

    protected Polygon shape;
    protected float outerRadius;

    public BasicPolygonPhysicsComponent(GameObject _gameObject, Polygon polygonShape) {
        super(_gameObject);
        changeShape(polygonShape);
    }

    public float calcOuterRadius() {
        Rectangle bnd = shape.getBoundingRectangle();
        return (float) Math.sqrt(bnd.getWidth() + bnd.getWidth() + bnd.getHeight() + bnd.getHeight());
    }

    public void changeShape(Polygon polygonShape) {
        shape = polygonShape;
        outerRadius = calcOuterRadius();
    }

    @Override
    public float getOuterRadius() {
        return outerRadius;
    }

    @Override
    public boolean doesCollide(PhysicsComponent physicsComponent2) {
        if (!doesOuterRadiusCollide(physicsComponent2)) return false;

        if (physicsComponent2 instanceof BasicPolygonPhysicsComponent) {
            return Intersector.overlapConvexPolygons(shape, ((BasicPolygonPhysicsComponent) physicsComponent2).shape);
        }
        return false;
    }
}
