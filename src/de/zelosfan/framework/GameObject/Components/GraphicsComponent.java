package de.zelosfan.framework.GameObject.Components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.GameObject.GameObject;
import de.zelosfan.framework.Util.Animation;

/**
 * Created by Zelos on 15.04.2015.
 */
public abstract class GraphicsComponent extends GameObjectComponent {

    enum DRAW_VARS {
        NORMAL(1, 1, 1, 1,  0),
        ZDRAW(1, 1, 1, 1, 0.5f);

        public final float positionXMult, positionYMult, sizeXMult, sizeYMult;
        public final float ZToYMultiplier; //multiply Z with SizeY and add it to Y;

        DRAW_VARS(float positionXMult, float positionYMult, float sizeXMult, float sizeYMult, float ZToYMultiplier) {
            this.positionXMult = positionXMult;
            this.positionYMult = positionYMult;
            this.sizeXMult = sizeXMult;
            this.sizeYMult = sizeYMult;

            this.ZToYMultiplier = ZToYMultiplier;
        }

        public int getDrawPositionX(float positionX, float sizeX) {
            return Math.round(positionX * positionXMult - (sizeX * sizeXMult) / 2);
        }

        public int getDrawPositionY(float positionY, float sizeY, float positionZ) {
            return Math.round(positionY * positionYMult - (sizeY * sizeYMult) / 2 + positionZ * ZToYMultiplier * sizeY);
        }

    }

    protected int sizeX, sizeY; //in pixel
    protected Color color;

    protected DRAW_VARS drawVariables = DRAW_VARS.NORMAL;

    protected Animation currentAnimation;
    protected int animiationTick = 0;

    protected ObjectMap<String, Animation> animations; //globally used reference


    public GraphicsComponent(GameObject _gameObject) {
        super(_gameObject);
        animations = gameObject.getAnimationMap();
    }

    public abstract void draw(TextureRegion texture, int x, int y, int sizeX, int sizeY, float rotation, Color color);

    public void draw(int x, int y, int sizeX, int sizeY) {
        draw(getCurrentTexture(), x, y, sizeX, sizeY, gameObject.getRotation(), color);
    }

    public void draw() {
        draw(getDrawPositionX(), getDrawPositionY(), sizeX, sizeY);
    }

    public int getDrawPositionX() {
        return drawVariables.getDrawPositionX(gameObject.getPosition().x, sizeX);
    }

    public int getDrawPositionY() {
        return drawVariables.getDrawPositionY(gameObject.getPosition().y, sizeY, gameObject.getPosition().z);
    }

    public TextureRegion getCurrentTexture() {
        return currentAnimation.getKeyFrame(animiationTick);
    }

    public Animation changeCurrentAnimation(String animationName) {
        currentAnimation = animations.get(animationName);
        return currentAnimation;
    }

    @Override
    public void render() {
        super.render();
        draw();
    }

    @Override
    public void receive(String message, Object... objects) {
        super.receive(message, objects);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    @Override
    public void disable() {
        super.disable();
    }

    @Override
    public void enable() {
        super.enable();
    }

    @Override
    public void tick() {
        super.tick();
        animiationTick++;
    }

    @Override
    public void dispose() {
    }
}
