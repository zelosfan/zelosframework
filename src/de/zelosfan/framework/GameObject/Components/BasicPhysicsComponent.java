package de.zelosfan.framework.GameObject.Components;

import de.zelosfan.framework.GameObject.GameObject;
import de.zelosfan.framework.GameObject.World;

/**
 * Created by Zelos on 21.04.2015.
 */
public class BasicPhysicsComponent extends PhysicsComponent {
    public BasicPhysicsComponent(GameObject _gameObject) {
        super(_gameObject);
    }

    public void move() {
        gameObject.getSpeed().add(gameObject.getAcceleration());

        if (!moveX()) {
            gameObject.getSpeed().x = 0; //restitiution??
            gameObject.getAcceleration().x = 0; // -gameObject.getAcceleration().x;
        }
        //moveY();

        gameObject.getSpeed().scl(1 - World.SPEED_FRICTION);
        gameObject.getAcceleration().scl(1 - World.ACCELERATION_FRICTION); //mass?
    }

    protected boolean moveX() {
        float prevXPos = gameObject.getPosition().x;

        gameObject.getPosition().x += gameObject.getSpeed().x;

        //check collision
    //    if () {
        //    gameObject.getPosition().x = prevXPos;
      //  }

        return true;
    }


    @Override
    public float getOuterRadius() {
        return 0;
    }

    @Override
    public boolean doesCollide(PhysicsComponent physicsComponent2) {
        return false;
    }

    @Override
    public boolean doesOuterRadiusCollide(PhysicsComponent physicsComponent2) {
        return super.doesOuterRadiusCollide(physicsComponent2);
    }

    @Override
    public void receive(String message, Object... objects) {
        super.receive(message, objects);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    @Override
    public void disable() {
        super.disable();
    }

    @Override
    public void enable() {
        super.enable();
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
