package de.zelosfan.framework.GameObject.Components;

import de.zelosfan.framework.GameObject.GameObject;

/**
 * Created by Zelos on 21.04.2015.
 */
public abstract class PhysicsComponent extends GameObjectComponent  {
    public PhysicsComponent(GameObject _gameObject) {
        super(_gameObject);
    }

    public abstract float getOuterRadius();

    public abstract boolean doesCollide(PhysicsComponent physicsComponent2);

    public boolean doesOuterRadiusCollide(PhysicsComponent physicsComponent2) {
        float dst = Math.abs(gameObject.getPosition().x - physicsComponent2.gameObject.getPosition().x) +
                Math.abs(gameObject.getPosition().y - physicsComponent2.gameObject.getPosition().y);

        return dst < getOuterRadius() + physicsComponent2.getOuterRadius();
    }

    public abstract void move();

    @Override
    public void receive(String message, Object... objects) {
        super.receive(message, objects);
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }


    @Override
    public void disable() {
        super.disable();
    }

    @Override
    public void enable() {
        super.enable();
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {

    }
}
