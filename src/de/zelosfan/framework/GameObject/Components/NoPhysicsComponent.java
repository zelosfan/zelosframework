package de.zelosfan.framework.GameObject.Components;

import de.zelosfan.framework.GameObject.GameObject;

/**
 * Created by Zelos on 21.04.2015.
 */
public class NoPhysicsComponent extends PhysicsComponent {


    public NoPhysicsComponent(GameObject _gameObject) {
        super(_gameObject);
    }


    @Override
    public float getOuterRadius() {
        return 0;
    }

    @Override
    public boolean doesCollide(PhysicsComponent physicsComponent2) {
        return false;
    }

    @Override
    public void move() {

    }

}
