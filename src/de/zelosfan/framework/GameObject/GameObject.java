package de.zelosfan.framework.GameObject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import de.zelosfan.framework.GameObject.Components.GameObjectComponent;
import de.zelosfan.framework.GameObject.Components.GraphicsComponent;
import de.zelosfan.framework.GameObject.Components.PhysicsComponent;
import de.zelosfan.framework.Interpolation.TweenHandler;
import de.zelosfan.framework.Util.Animation;
import de.zelosfan.framework.Util.Message;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Zelos on 15.04.2015.
 */
public abstract class GameObject implements Pool.Poolable{
    protected Vector3 position; //middle of the object
    protected Vector3 speed;
    protected Vector3 acceleration;
    protected float rotation = 0f; //in degrees

    protected boolean enabled = true;
    protected boolean remove = false;
    protected boolean initialized = false;

    protected TweenHandler tweenHandler;

    protected GraphicsComponent graphicsComponent;
    protected PhysicsComponent physicsComponent;
    protected GameObjectComponent[] component;

    private Queue<Message> eventQueue;

    public GameObject() {
        position = World.vector3Pool.obtain();
        speed = World.vector3Pool.obtain();
        acceleration = World.vector3Pool.obtain();

        tweenHandler = new TweenHandler();
        eventQueue = new LinkedList<>();
    }

    public void initialize(GameObjectPack initPack) {
        position.x = initPack.positionX;
        position.y = initPack.positionY;
        position.z = initPack.positionZ;

        speed.x = initPack.speedX;
        speed.y = initPack.speedY;
        speed.z = initPack.speedZ;

        acceleration.x = initPack.accelerationX;
        acceleration.y = initPack.accelerationY;
        acceleration.z = initPack.accelerationZ;

        graphicsComponent = initPack.graphicsComponent;
        physicsComponent = initPack.physicsComponent;
        component = initPack.components;

        initialized = true;
    }

    //don't create a new one pass the EntityTyp animiaton reference
    public abstract ObjectMap<String, Animation> getAnimationMap();

    @Override
    public void reset() {
        initialized = false;

        tweenHandler.removeAll();

        rotation = 0;

        position.x = 0;
        position.y = 0;
        position.z = 0;

        speed.x = 0;
        speed.y = 0;
        speed.z = 0;

        acceleration.x = 0;
        acceleration.y = 0;
        acceleration.z = 0;

        tweenHandler.add("X", 0, 0, 0, Interpolation.circle);
        tweenHandler.add("Y", 0, 0, 0, Interpolation.circle);
        tweenHandler.add("Z", 0, 0, 0, Interpolation.circle);

        tweenHandler.add("rotation", 0, 0, 0, Interpolation.circle);

        tweenHandler.add("r", 0, 0, 0, Interpolation.circle);
        tweenHandler.add("g", 0, 0, 0, Interpolation.circle);
        tweenHandler.add("b", 0, 0, 0, Interpolation.circle);
        tweenHandler.add("a", 0, 0, 0, Interpolation.circle);

        eventQueue.clear();
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isRemove() {
        return remove;
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    public GameObject copy() {
        GameObjectPack gameObjectPack = new GameObjectPack(position.x, position.y, position.z, speed.x, speed.y, speed.z, acceleration.x, acceleration.y, acceleration.z,
                rotation, null, null, null);
        return copy(gameObjectPack);
    }

    public abstract GameObject copy(GameObjectPack gameObjectPark);

    public void tick() {
        processEventQueue();

        graphicsComponent.tick();
    }

    public void physicsTick() {
        processEventQueue();

        physicsComponent.tick();
    }

    public void render() {
        graphicsComponent.render();
    }

    public void processEventQueue() {
        while (eventQueue.iterator().hasNext()) {
            Message next =  eventQueue.iterator().next();
            receive(next.event, next.objects);
            eventQueue.iterator().remove();
            World.gameObjectMessages.free(next);
        }
    }

    public void receive(String event, Object... objects) {

    }

    public void sendMessage(String event, Object... objects) {
        eventQueue.add(World.gameObjectMessages.obtain().set(event, objects));
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public Vector3 getPosition() {
        return position;
    }

    public void setPosition(Vector3 position) {
        this.position = position;
    }

    public Vector3 getSpeed() {
        return speed;
    }

    public void setSpeed(Vector3 speed) {
        this.speed = speed;
    }

    public Vector3 getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Vector3 acceleration) {
        this.acceleration = acceleration;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
}
