package de.zelosfan.framework.Timing;

import com.badlogic.gdx.Application;
import de.zelosfan.framework.Logging.LogManager;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 10.05.13
 * Time: 13:26
 */
public class GametimeManager implements GametimeHandler{

    private long TICKCOUNT = 1; //TICK ACCUMULATOR
    private int TPS = 100; //TICKS PER SECOND

    private double deltaCollector = 0; //RENDER DELTA ACCUMULATOR

    @Override
    public void setTPS(int _tps) {
        TPS = _tps;
    }

    @Override
    public long getTotalTickCount() {
        return TICKCOUNT;
    }

    @Override
    public int getTPS() {
        return TPS;
    }

    @Override
    public void updateDeltaTime(double delta) {
        if (delta >= 0) {
            deltaCollector += delta;
        } else {
            LogManager.log(GametimeManager.class, Application.LOG_ERROR, "(updateTime) Delta is not valid: + delta");
        }
    }

    @Override
    public int realtimeToGametime(double seconds) {
        return (int) Math.round(seconds * TPS);
    }

    @Override
    public double gametimeToRealtime(int ticks) {
        return ((double) ticks) / TPS;
    }

    @Override
    public boolean tick() {
        if (deltaCollector >= (1f / TPS)) {
            deltaCollector -= (1f / TPS);

            TICKCOUNT++;
            if (TICKCOUNT == Long.MAX_VALUE) {
                TICKCOUNT = 1;
            }

            return true;
        }
        return false;
    }

}
