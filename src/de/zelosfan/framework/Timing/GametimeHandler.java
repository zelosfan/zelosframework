package de.zelosfan.framework.Timing;

/**
 * Created by Zelos on 13.04.2015.
 */
public interface GametimeHandler {

    void setTPS(int _tps);

    long getTotalTickCount();
    int getTPS();

    int realtimeToGametime(double seconds);
    double gametimeToRealtime(int ticks);

    void updateDeltaTime(double delta);
    boolean tick(); // returns true while tick procedures should be called


}
