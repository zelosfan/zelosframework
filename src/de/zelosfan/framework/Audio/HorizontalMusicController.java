package de.zelosfan.framework.Audio;

import de.zelosfan.framework.Service.ServiceLocator;

import java.util.ArrayList;

/**
 * Created by Zelos on 12.04.2015.
 */
public class HorizontalMusicController {

    enum FadeType {
        XFade, XCircleFade, NoFade
    }

    class MusicChannelBundle {
        final int channel1;
        final int channel2;
        final FadeType fadeType;

        final float fadeDistancePercent;

        public MusicChannelBundle(int _channel1, int _channel2, FadeType _fadeType, float fadeDstPercent) {
            channel1 = _channel1;
            channel2 = _channel2;
            fadeType = _fadeType;
            fadeDistancePercent = fadeDstPercent;
        }
    }

    ArrayList<MusicChannelBundle> musicChannelBundles;

    public HorizontalMusicController() {
        musicChannelBundles = new ArrayList<>();
    }

    public void tick() {
        for (MusicChannelBundle channelBundle: musicChannelBundles) {
          //  ServiceLocator.audioEngine. need total music length
        }
    }

}
