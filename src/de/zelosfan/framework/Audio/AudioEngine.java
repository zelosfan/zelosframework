package de.zelosfan.framework.Audio;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.LinkedList;

/**
 * Created by Zelos on 12.04.2015.
 */
public interface AudioEngine {

    /*
        The AudioEngine should use the "Sound"-Interface to represent Sound(-files)

        Vars:
            initialised - instance properly initialised
            volumes - 0.0 mute - 1.0 max volume
            channelNr - number of vertical Music channels


        Structure:
            Map to associate Soundnames(String) to Sounds
            List of current MusicTrack chain - refills current queue if empty
            Current Queue of MusicFiles to play - current being first
            Stack of MusicFiles of last played tracks - ontop is the last track played
            List of all Sound/SoundInstances Bundle ever created

        Procedures:

            tick:
                - check if musicTrack is over -> start next track in queue (refill queue with chain)


     */

    boolean isInitialized();
    void initialize(ObjectMap<String, Sound> _sfxMap);

    int getMusicChannelNumber();

    void addAllToMusicChain(int channel, LinkedList<FileHandle> list);

    void muteMusic(boolean isMute); //muted music is not played back
    void muteSFX(boolean isMute); //muted sounds are not played back / no Sound instance created

    void changeVolumeSFX(float sfxVolume); //changes all future SFX volume
    void changeVolumeMusic(int channel, float musicVolume); //changes current track and all future track volume for this channel
    void changeDefaultMusicVolume(float musicVolume); //changes the default music volume

    void setMusicVolumeInterpolation(int channel, float startVolume, float destinationVolume, int duration, Interpolation interpolation);

    Sound getSound(String soundName); //getSound by Name according to the sfxMap
    Sound getSound(long ID);  //getSound by Name according to the instanceID
    String getSoundName(Sound sound); //get the SoundName according to the Sound

    MusicState getMusicState(int channel);

    long playSound(String soundName); //play Sound with soundName (be careful! low performance since Objectmap)
    long playSound(Sound sound); // return SoundInstanceUID - unique for every playback [CAN BE 0]
    long playSound(Sound sound, float pan); //@pan sfx left/right balance speaker -1 left 0 normal 1 right
    long playSound(Sound sound, float pan, float pitch, float volume); // specific pitch & volume / overrides default

    void stopAllSounds();

    void soundIDstop(long soundID);
    void soundIDpause(long soundID);
    void soundIDresume(long soundID);
    void soundIDsetVolume(long soundID, float volume);
    void soundIDsetPitch(long soundID, float pitch);
    void soundIDsetLoopind(long soundID, boolean loop);

    void lastMusicTrack(int channel); //play last finished track
    void nextMusicTrack(int channel); //skip current track / playNextTrack

    void playMusicQueue(int channel); //start music playback
    void pauseMusicQueue(int channel); //pause track at current location
    void stopMusicQueue(int channel);  // stop music playback - stopped/current track next in queue

    void tick();

    void dispose(); //clean up ALL sfx and music references - sets initalized to false
}
