package de.zelosfan.framework.Audio;

/**
 * Created by Zelos on 12.04.2015.
 */
public enum MusicState {
    PLAYING, PAUSED, STOPPED
}
