package de.zelosfan.framework.Audio;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Interpolation.TweenHandler;
import de.zelosfan.framework.Logging.LogManager;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Audio playback using LibGDX implementations
 *
 * Created by Zelos on 12.04.2015.
 */
public class Audiomanager implements AudioEngine {


    public static int MUSIC_CHANNEL_COUNT = 4;

    public static LinkedList<FileHandle> getMusicFiles(FileHandle dirHandle) {
        LinkedList<FileHandle> fileHandles = new LinkedList<>();
        for (FileHandle fileHandle: dirHandle.list()) {
            if (fileHandle.extension().equals("wav") || fileHandle.extension().equals("mp3") || fileHandle.extension().equals("ogg")) {
                fileHandles.add(fileHandle);
            }
        }

        return fileHandles;
    }

    class SoundPack {
        public final Sound sound;
        public final Long ID;

        public SoundPack(Sound _sound, long _ID) {
            sound = _sound;
            ID = _ID;
        }
    }

    LinkedList<FileHandle>[] musicChain; //refills queue if queue is empty
    Queue<FileHandle>[] musicQueue; //next musictracks to be played - first is current
    Stack<FileHandle>[] lastPlayedTracks; //past played tracks

    TweenHandler[] tweenHandlers; //interpolates between values of volume, pitch and pan for each given channel

    ObjectMap<String, Sound> sfxMap; // sfxName to soundFile map - set in init
    LinkedList<SoundPack> playingSoundIDs; // all active SoundIDS

    Music[] currentTrack;
    MusicState[] currentMusicState;

    boolean musicMuted = false;
    boolean sfxMuted = false;

    Float[] channelVolume;
    float sfxVolume = 0.5f;
    float musicVolume = 0.5f;

    boolean initialized = false;


    public Audiomanager() {
        musicChain = new LinkedList[MUSIC_CHANNEL_COUNT];
        musicQueue = new LinkedList[MUSIC_CHANNEL_COUNT];
        lastPlayedTracks = new Stack[MUSIC_CHANNEL_COUNT];
        channelVolume = new Float[MUSIC_CHANNEL_COUNT];
        tweenHandlers = new TweenHandler[MUSIC_CHANNEL_COUNT];
        currentTrack = new Music[MUSIC_CHANNEL_COUNT];
        currentMusicState = new MusicState[MUSIC_CHANNEL_COUNT];

        for (int i = 0; i < MUSIC_CHANNEL_COUNT; i++) {
            musicQueue[i] = new LinkedList<>();
            musicChain[i] = new LinkedList<>();
            lastPlayedTracks[i] = new Stack<>();
            channelVolume[i] = musicVolume;
            tweenHandlers[i] = new TweenHandler();

            tweenHandlers[i].add("volume", sfxVolume, sfxVolume, 0, Interpolation.circle);
            tweenHandlers[i].add("pitch", 1, 1, 0, Interpolation.circle);
            tweenHandlers[i].add("pan", 0.5f, 0.5f, 0, Interpolation.circle);

            currentMusicState[i] = MusicState.STOPPED;

            currentTrack[i] = null;
        }
    }

    @Override
    public int getMusicChannelNumber() {
        return MUSIC_CHANNEL_COUNT;
    }

    public void initialize(ObjectMap<String, Sound> _sfxMap) {
        sfxMap = _sfxMap;
        playingSoundIDs = new LinkedList<>();

        initialized = true;
    }

    public void replaceMusicChain(int channel, LinkedList<FileHandle> musicChain) {
        this.musicChain[channel] = musicChain;
    }

    public void addAllToMusicChain(int channel, LinkedList<FileHandle> list) {
        musicChain[channel].addAll(list);
    }

    public void addToMusicChain(int channel, FileHandle track) {
        musicChain[channel].add(track);
    }

    @Override
    public boolean isInitialized() {
        return initialized;
    }

    @Override
    public void muteMusic(boolean isMute) {
        musicMuted = isMute;
    }

    @Override
    public void muteSFX(boolean isMute) {
        sfxMuted = isMute;
    }

    @Override
    public void changeVolumeSFX(float sfxVolume) {
        this.sfxVolume = sfxVolume;
    }

    @Override
    public void setMusicVolumeInterpolation(int channel, float startVolume, float destinationVolume, int duration, Interpolation interpolation) {
        tweenHandlers[channel].add("volume", startVolume, destinationVolume, duration, interpolation);
    }

    @Override
    public void changeVolumeMusic(int channel, float musicVolume) {
        channelVolume[channel] = musicVolume;
        currentTrack[channel].setVolume(musicVolume);
        tweenHandlers[channel].add("volume", musicVolume, musicVolume, 0, Interpolation.circle);
    }

    @Override
    public void changeDefaultMusicVolume(float musicVolume) {
        this.musicVolume = musicVolume;
    }

    @Override
    public void stopAllSounds() {
        /*for (Sound sound : sfxMap.values().iterator()) {
            sound.stop();
        }*/
    }

    @Override
    public long playSound(String soundName) {
        return playSound(getSound(soundName), 0.5f);
    }

    @Override
    public long playSound(Sound sound) {
        return playSound(sound, 0.5f);
    }

    @Override
    public long playSound(Sound sound, float pan) {
        return playSound(sound, pan, 1, sfxVolume);
    }

    @Override
    public long playSound(Sound sound, float pan, float pitch, float volume) {
        if (!initialized) {
            LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(playSound) AudioManager not initialized");
            return 0;
        }

        if (sfxMuted) {
            return 0;
        }

        if (sound == null) {
            LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(playSound) sound is NULL");
            return 0;
        }

        if (pan < -1 || pan > 1) {
            LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(playSound) pan(" + pan + ") is not in range -1 to 1");
        }

        if (pitch < 0.5f || pitch > 2) {
            LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(playSound) pitch(" + pitch + ") is not in range 0,5f to 2");
        }

        if (!sfxMuted) {
            long soundID = sound.play(volume, Math.min(Math.max(0.5f ,pitch), 2), Math.min(Math.max(-1, pan), 1));

            if (soundID != 0) {
                playingSoundIDs.add(new SoundPack(sound, soundID));
            }

            return soundID;
        }

        return 0;
    }

    @Override
    public Sound getSound(long ID) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (ID == soundPack.ID) {
                return soundPack.sound;
            }
        }
        return null;
    }

    @Override
    public String getSoundName(Sound sound) {
        return sfxMap.findKey(sound, true);
    }

    @Override
    public Sound getSound(String soundName) {
        return sfxMap.get(soundName);
    }

    @Override
    public void soundIDstop(long soundID) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.stop(soundID);
                break;
            }
        }
    }

    @Override
    public void soundIDpause(long soundID) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.pause(soundID);
                break;
            }
        }
    }

    @Override
    public void soundIDresume(long soundID) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.resume(soundID);
                break;
            }
        }
    }

    @Override
    public void soundIDsetVolume(long soundID, float volume) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.setVolume(soundID, volume);
                break;
            }
        }
    }

    @Override
    public void soundIDsetPitch(long soundID, float pitch) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.setPitch(soundID, pitch);
                break;
            }
        }
    }

    @Override
    public void soundIDsetLoopind(long soundID, boolean loop) {
        for (SoundPack soundPack : playingSoundIDs) {
            if (soundID == soundPack.ID) {
                soundPack.sound.setLooping(soundID, loop);
                break;
            }
        }
    }

    @Override
    public void lastMusicTrack(int channel) {
        if (!lastPlayedTracks[channel].isEmpty()) {
            stopMusicQueue(channel);
            musicQueue[channel].clear();

            musicQueue[channel].add(lastPlayedTracks[channel].pop());

            playMusicQueue(channel);
        }
    }


    @Override
    public void nextMusicTrack(int channel) {
        if (musicMuted) return;

        if (currentTrack[channel] != null) {
            lastPlayedTracks[channel].add(musicQueue[channel].remove());
            currentTrack[channel].dispose();
        }

        if (musicQueue[channel].isEmpty()) {
            if (musicChain[channel].isEmpty()) {
                //LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(skipMusicTrack) musicChain is empty");
                currentMusicState[channel] = MusicState.STOPPED;
                return;
            }

            for (FileHandle fileHandler: musicChain[channel]) {
                musicQueue[channel].add(fileHandler);
            }
        }

        if (currentMusicState[channel] == MusicState.PLAYING) {
            currentTrack[channel] = Gdx.audio.newMusic(musicQueue[channel].peek());
            currentTrack[channel].setVolume(musicVolume);
            currentTrack[channel].play();
        }
    }

    @Override
    public void playMusicQueue(int channel) {
        if (musicMuted) return;

        if (currentTrack[channel] == null) {
            nextMusicTrack(channel);
        } else {
            if (!currentTrack[channel].isPlaying()) {
                currentTrack[channel].play();
            }
        }

        if (currentTrack[channel] != null) {
            if (currentTrack[channel].isPlaying()) {
                currentMusicState[channel] = MusicState.PLAYING;
            } else {
                LogManager.log(Audiomanager.class, Application.LOG_ERROR, "(playMusicQueue) Music not started!?");
            }
        }

    }

    @Override
    public void pauseMusicQueue(int channel) {
        if (currentTrack[channel] != null) {
            if (currentTrack[channel].isPlaying()) {
                currentTrack[channel].pause();

                currentMusicState[channel] = MusicState.PAUSED;
            }
        } else {
            nextMusicTrack(channel);
        }
    }

    @Override
    public MusicState getMusicState(int channel) {
        return currentMusicState[channel];
    }

    @Override
    public void stopMusicQueue(int channel) {
        if (currentTrack[channel] == null) {
            return;
        }

        currentTrack[channel].stop();

        currentMusicState[channel] = MusicState.STOPPED;
    }



    @Override
    public void tick() {
        if (!initialized) return;

        for (int i = 0; i < MUSIC_CHANNEL_COUNT; i++) {
            tweenHandlers[i].tick();


            switch (currentMusicState[i]) {

                case PLAYING:
                    if (currentTrack[i] != null) {
                        if (!currentTrack[i].isPlaying()) {
                            nextMusicTrack(i);
                        } else {
                            changeVolumeMusic(i, tweenHandlers[i].get("volume"));
                            currentTrack[i].setPan(tweenHandlers[i].get("pan"), tweenHandlers[i].get("volume"));
                        }
                    } else {
                        nextMusicTrack(i);
                    }
                    break;



                case PAUSED:
                    if (currentTrack[i] != null) {
                        if (currentTrack[i].isPlaying()) {
                            pauseMusicQueue(i);
                        }
                    } else {
                        nextMusicTrack(i);
                    }
                    break;

                case STOPPED:
                    if (currentTrack[i] != null) {
                        if (currentTrack[i].isPlaying()) {
                            stopMusicQueue(i);
                        }
                    } else {
                        nextMusicTrack(i);
                    }
                    break;
            }
        }
    }

    @Override
    public void dispose() {
        for (int i = 0; i < MUSIC_CHANNEL_COUNT; i++) {
            if (currentTrack[i] != null) {
                currentTrack[i].stop();
                currentTrack[i].dispose();
            }
        }
        for (Sound sound : sfxMap.values().iterator()) {
            sound.stop();
            sound.dispose();
        }
    }
}