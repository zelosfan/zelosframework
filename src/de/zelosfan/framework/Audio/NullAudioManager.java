package de.zelosfan.framework.Audio;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.LinkedList;

/**
 * Created by Zelos on 14.04.2015.
 */
public class NullAudioManager implements AudioEngine {

    @Override
    public void initialize(ObjectMap<String, Sound> _sfxMap) {

    }

    @Override
    public void addAllToMusicChain(int channel, LinkedList<FileHandle> list) {

    }

    @Override
    public boolean isInitialized() {
        return true;
    }

    @Override
    public int getMusicChannelNumber() {
        return 0;
    }

    @Override
    public void muteMusic(boolean isMute) {

    }

    @Override
    public void muteSFX(boolean isMute) {

    }

    @Override
    public void changeVolumeSFX(float sfxVolume) {

    }

    @Override
    public void changeVolumeMusic(int channel, float musicVolume) {

    }

    @Override
    public void changeDefaultMusicVolume(float musicVolume) {

    }

    @Override
    public void setMusicVolumeInterpolation(int channel, float startVolume, float destinationVolume, int duration, Interpolation interpolation) {

    }

    @Override
    public Sound getSound(String soundName) {
        return null;
    }

    @Override
    public Sound getSound(long ID) {
        return null;
    }

    @Override
    public String getSoundName(Sound sound) {
        return null;
    }

    @Override
    public MusicState getMusicState(int channel) {
        return null;
    }

    @Override
    public long playSound(String soundName) {
        return 0;
    }

    @Override
    public long playSound(Sound sound) {
        return 0;
    }

    @Override
    public long playSound(Sound sound, float pan) {
        return 0;
    }

    @Override
    public long playSound(Sound sound, float pan, float pitch, float volume) {
        return 0;
    }

    @Override
    public void stopAllSounds() {

    }

    @Override
    public void soundIDstop(long soundID) {

    }

    @Override
    public void soundIDpause(long soundID) {

    }

    @Override
    public void soundIDresume(long soundID) {

    }

    @Override
    public void soundIDsetVolume(long soundID, float volume) {

    }

    @Override
    public void soundIDsetPitch(long soundID, float pitch) {

    }

    @Override
    public void soundIDsetLoopind(long soundID, boolean loop) {

    }

    @Override
    public void lastMusicTrack(int channel) {

    }

    @Override
    public void nextMusicTrack(int channel) {

    }

    @Override
    public void playMusicQueue(int channel) {

    }

    @Override
    public void pauseMusicQueue(int channel) {

    }

    @Override
    public void stopMusicQueue(int channel) {

    }

    @Override
    public void tick() {

    }

    @Override
    public void dispose() {

    }
}
