package de.zelosfan.framework.IO;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Util.Animation;
import de.zelosfan.framework.Util.MathUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 09.05.13
 * Time: 21:23
 */
public class ResourceLoader extends AssetManager implements Disposable {

    private ArrayList<AssetLoaderTask> assetLoaderTasks;
    public static LinkedList<TextureAtlas> atlanten = new LinkedList<>();


    @Override
    public synchronized void dispose() {
        super.dispose();
        assetLoaderTasks.clear();
        disposeObjectMap(Animation.animationList);
//        disposeObjectMap(Particle.particleDefinitionList);
    }

    public ResourceLoader() {
        super();
        assetLoaderTasks = new ArrayList<>();
    }


    @Override
    public synchronized boolean update() {
        if (super.update()) {
            onFinishLoading();
            return true;
        }
        return false;
    }

    private void onFinishLoading() {
        for (AssetLoaderTask assetLoaderTask : assetLoaderTasks) {
            LogManager.log(this.getClass(), Application.LOG_DEBUG, "(onFinishLoading) Trying to process assetTasks " + assetLoaderTask.packname);
            switch (assetLoaderTask.taskTyp) {
                //Put Animations from loaded TextureAtlas into the Animation.animationList
                case 1:
                    TextureAtlas textureAtlas = get(assetLoaderTask.packname, TextureAtlas.class);
                    atlanten.add(textureAtlas);
                    ObjectMap<String, LinkedList<TextureRegion>> animationPreface = new ObjectMap<>();

                    String tname;
                    int tposition;
                    LinkedList<TextureRegion> textureRegions;

                    for (TextureAtlas.AtlasRegion atlasRegion : textureAtlas.getRegions()) {
                        assetLoaderTask.hashMap.put(atlasRegion.name, atlasRegion);

                        if (atlasRegion.name.matches("Anim.*")) {
                            tname = atlasRegion.name.toLowerCase().replaceAll("[0-9]", "");
                            tname = tname.replaceAll("anim", "");
                            tposition = Integer.parseInt(atlasRegion.name.toLowerCase().replaceAll("[a-z]", ""));

                            if (!animationPreface.containsKey(tname)) {
                                textureRegions = new LinkedList<>();
                                animationPreface.put(tname, textureRegions);
                            } else {
                                textureRegions = animationPreface.get(tname);
                            }

                            textureRegions.add(tposition - 1, atlasRegion);
                        }

                    }
                    Iterator<LinkedList<TextureRegion>> it = animationPreface.values().iterator();
                    Iterator<String> it2 = animationPreface.keys().iterator();
                    while (it.hasNext()) {
                        LinkedList<TextureRegion> textureRegions1 = it.next();
                        String name = it2.next();
                        TextureRegion[] tex = new TextureRegion[textureRegions1.size()];
                        for (int i = 0; i < textureRegions1.size(); i++) {
                            tex[i] = textureRegions1.get(i);
                        }

                        Animation.animationList.put(name, new Animation(tex));
                    }
                    break;
                //######################


                //Put Integers from the loaded Texture into the result HashMap
                case 2:
                    TextureAtlas textureAtlas2 = get(assetLoaderTask.packname, TextureAtlas.class);

                    for (TextureAtlas.AtlasRegion atlasRegion : textureAtlas2.getRegions()) {
                        Integer[][] intgers = new Integer[atlasRegion.getRegionWidth()][atlasRegion.getRegionHeight()];

                        Texture atTexture = atlasRegion.getTexture();
                        atTexture.getTextureData().prepare();
                        Pixmap pixmap = atTexture.getTextureData().consumePixmap();

                        for (int x = 0; x < atlasRegion.getRegionWidth(); x++) {
                            for (int y = 0; y < atlasRegion.getRegionHeight(); y++) {
                                intgers[x][y] = pixmap.getPixel(atlasRegion.getRegionX() + x, atlasRegion.getRegionY() + y);
                            }
                        }
                        assetLoaderTask.hashMap.put(atlasRegion.name, intgers);
                    }
                    break;
                //##############################

                //Put the loaded TextureAtlas into the result Hashmap
                case 3:
                    if (this.isLoaded(assetLoaderTask.packname)) {
                        assetLoaderTask.hashMap.put(assetLoaderTask.packname.replaceAll(".*/", ""), get(assetLoaderTask.packname));
                    } else {
                        LogManager.log(ResourceLoader.class, Application.LOG_ERROR, "(onFinishLoading) Inconsistent loading of " + assetLoaderTask.packname);
                    }
                    break;

                //############################


                //Put the different fontSizes in the corresponding 
                case 4:
                    TextureAtlas textureAtlas3 = get(assetLoaderTask.packname, TextureAtlas.class);

                    for (TextureAtlas.AtlasRegion atlasRegion : textureAtlas3.getRegions()) {
                        int size = Integer.parseInt(atlasRegion.name.toLowerCase().replaceAll("[a-z]", ""));
                        String name = atlasRegion.name.toLowerCase().replaceAll("[0-9]", "");

                        if (assetLoaderTask.hashMap.get(name) == null) {
                            ObjectMap<Integer, BitmapFont> fontObjectMap = new ObjectMap<>();
                            assetLoaderTask.hashMap.put(name, fontObjectMap);
                        }

                        BitmapFont bitmapFont = new BitmapFont(Gdx.files.internal("font/" + atlasRegion.name + ".fnt"), atlasRegion, false);
                        ((ObjectMap) assetLoaderTask.hashMap.get(name)).put(size, bitmapFont);
                    }
                    break;

                //###########################
            }
        }
        assetLoaderTasks.clear();
    }

    public BitmapFont createTTFFont(FileHandle fileHandle, int fontSize) {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter1 = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter1.size = fontSize;
        return createTTFFont(fileHandle, parameter1);
    }

    public BitmapFont createTTFFont(FileHandle fileHandle, FreeTypeFontGenerator.FreeTypeFontParameter parameter) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fileHandle);
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();
        return font;
    }

    public static void disposeObjectMap(ObjectMap objectMap) {
        for (Object obj : objectMap.values()) {
            if (obj instanceof Disposable) {
                ((Disposable) obj).dispose();
            }
            if (obj instanceof ObjectMap) {
                disposeObjectMap((ObjectMap) obj);
            }
        }
        objectMap.clear();
    }

    public void loadImagePackIntoTextureRegions(String packname, ObjectMap<String, TextureRegion> hashMap) {
        if (Gdx.files.internal(packname).exists()) {
            assetLoaderTasks.add(new AssetLoaderTask(1, packname, hashMap));
            this.load(packname, TextureAtlas.class);
        }
    }

    public void loadImagePackIntoPixelInteger(String packname, ObjectMap<String, Integer[][]> hashMap) {
        if (Gdx.files.internal(packname).exists()) {
            assetLoaderTasks.add(new AssetLoaderTask(2, packname, hashMap));
            this.load(packname, TextureAtlas.class);
        }
    }

  /*  //compositeSize: size of the whole Multi image structure
    public Texture[][] loadMultiImage(FileHandle fileHandle, int compositeSizeX, int compositeSizeY) {
        FileHandle[] files = fileHandle.list();
        FileHandle[] ordered = new FileHandle[files.length];

        for (FileHandle f: files) {
            get
        }



    }*/

    public <T> void loadDirectoryFiles(String path, ObjectMap hashMap, Class<T> type, boolean recursive) {
        if (Gdx.files.internal(path).exists()) {
            FileHandle dirHandle = Gdx.files.internal(path);
            for (FileHandle entry : dirHandle.list()) {
                if (entry.isDirectory() && recursive) {
                    loadDirectoryFiles(entry.path() + "/", hashMap, type, true);
                } else {
                    if (!entry.extension().equals("ini")) {
                        assetLoaderTasks.add(new AssetLoaderTask(3, path + entry.name(), hashMap));
                        this.load(path + entry.name(), type);
                    }
                }
            }
        }
    }

    public <T> void loadDirectoryFiles(String path, ObjectMap hashMap, Class<T> type) {
        loadDirectoryFiles(path, hashMap, type, false);
    }

    public static void loadShaders(String path, ObjectMap<String, ShaderProgram> shaderMap) {
        FileHandle dirHandle = Gdx.files.internal(path);
        if (dirHandle != null) {

            for (FileHandle entry : dirHandle.list()) {
                if (entry.extension().equals("vert")) {
                    shaderMap.put(entry.nameWithoutExtension(), new ShaderProgram(Gdx.files.internal(entry.path()), Gdx.files.internal(entry.pathWithoutExtension() + ".frag")));
                    if (!shaderMap.get(entry.nameWithoutExtension()).isCompiled()) {
                        LogManager.log(ResourceLoader.class, Application.LOG_ERROR, "(loadShaders) compilation failed:\n" + shaderMap.get(entry.nameWithoutExtension()).getLog());
                    }
                }
            }
        } else {
            throw new RuntimeException(path+" (dir) not found");
        }
    }

    /*pub77lic static void loadSkeleton(String path, TextureAtlas animationAtlas, ObjectMap<String, SkeletonData> skeletonMap) {
        if (Gdx.files.internal(path).exists()) {
            FileHandle dirHandle = Gdx.files.internal(path);
            SkeletonJson json = new SkeletonJson(animationAtlas);
            for (FileHandle entry : dirHandle.list()) {
                if (entry.extension().equals("json")) {
                    skeletonMap.put(entry.nameWithoutExtension(), json.readSkeletonData(Gdx.files.internal(entry.path())));
                }
            }
        }
    } */

    public void loadFontSizes(String packname, ObjectMap<String, ObjectMap<Integer, BitmapFont>> hashMap) {
        if (Gdx.files.internal(packname).exists()) {
            assetLoaderTasks.add(new AssetLoaderTask(4, packname, hashMap));
            this.load(packname, TextureAtlas.class);
        }
    }

    public static void loadShapeDefinitions(String filepath, ObjectMap<String, float[]> shapeMap) {
        JsonValue value = new JsonReader().parse(Gdx.files.internal(filepath)).get("shapes");
        for (JsonValue entry = value.child(); entry != null; entry = entry.next()) {

            ArrayList<Float> vertices = new ArrayList<>();
            for (JsonValue point = entry.get("points").child(); point != null; point = point.next()) {
               vertices.add(point.getFloat("x"));
               vertices.add(point.getFloat("y"));
            }

            for (JsonValue n = entry.get("texturename").child(); n != null; n = n.next()) {
                shapeMap.put(n.getString("n"), MathUtil.convertFloats(vertices));
            }

        }
    }
}
