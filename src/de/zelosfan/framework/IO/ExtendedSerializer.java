package de.zelosfan.framework.IO;

import com.badlogic.gdx.utils.ObjectMap;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 02.01.14
 * Time: 17:31
 */
public abstract class ExtendedSerializer<T> extends Serializer <T>{

    private int currentVersion;

    public ExtendedSerializer(int _currentVersion) {
        super();
        currentVersion = _currentVersion;
    }

    protected class SaveData {
        public ObjectMap<String, Object> data;
        public int version;

        public SaveData(int _version) {
            version = _version;
            data = new ObjectMap<>();
        }
    }

    @Override
    public void write(Kryo kryo, Output output, T t) {
        output.writeInt(currentVersion);
    }

    @Override
    public T read(Kryo kryo, Input input, Class<T> tClass) {
        int version = input.readInt();

        SaveData saveData = readFromVersion(version, kryo, input, tClass);

        boolean finished = saveData.version == version;
        while (!finished) {
            saveData = convert(saveData);
            if ((saveData.version == version) || (saveData.version == currentVersion)) {
                finished = true;
            }
            version = saveData.version;
        }

        return createObj(saveData);
    }

    public abstract T createObj(SaveData saveData);

    /**
     * Creates a SaveData Object which is able to create the required object
     * when passed to the createObj function
     *
     * @param version the version in which the loaded file is.
     * @param kryo the supplied kryo object.
     * @param input the input stream for reading purposes.
     * @param tClass the class object of the supplied class to read.
     * @return the SaveData Object with the expected contents for createObj().
     */
    public abstract SaveData readFromVersion(int version, Kryo kryo, Input input, Class<T> tClass);

    /**
     * Converts the supplied saveData Obj into the next higher version (+1)
     *
     * @param saveData the SaveData that is to be upgraded to the next version.
     * @return the upgraded SaveData object
     */
    public abstract SaveData convert(SaveData saveData);
}
