package de.zelosfan.framework.IO;

import com.badlogic.gdx.utils.ObjectMap;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 09.05.13
 * Time: 21:45
 */
public class AssetLoaderTask {
    public final String packname;
    public final ObjectMap hashMap;
    public final int taskTyp;

    public AssetLoaderTask(int taskTyp, String packname, ObjectMap hashMap) {
        this.taskTyp = taskTyp;
        this.packname = packname;
        this.hashMap = hashMap;
    }
}
