package de.zelosfan.framework.IO;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import de.zelosfan.framework.Logging.LogManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 11.05.13
 * Time: 14:48
 */
public class IOManager implements Disposable{

    public Kryo serializer;
    public ArrayList<Object> toWrite;

    public boolean PROCESS_COMPRESSED_FILE_IO = true;

    public void initialize() {
        if (serializer == null) {
            serializer = new Kryo();
            toWrite = new ArrayList<>();
        }
    }

    public void writeObject(Object obj) {
        toWrite.add(obj);
    }

    public void saveFile(FileHandle fileHandle) {
        saveFile(fileHandle, true);
    }

    public boolean noObjectsToWrite() {
        return toWrite.isEmpty();
    }


    public void saveFile(FileHandle fileHandle, boolean writeClazz) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Output output;

        if (PROCESS_COMPRESSED_FILE_IO) {
            DeflaterOutputStream outputStream = new DeflaterOutputStream(out);
            output = new Output(outputStream);
        } else {
            output = new Output(out);
        }

        LogManager.log(IOManager.class, Application.LOG_DEBUG, "(saveFile) Try to save to file -> " + fileHandle.path());
        for (Object obj : toWrite) {
            if (writeClazz) {
              serializer.writeClassAndObject(output, obj);
            } else {
              serializer.writeObject(output, obj);
            }
            LogManager.log(IOManager.class, Application.LOG_DEBUG, "Saving " + obj.getClass().getSimpleName());
        }
        output.close();

        fileHandle.writeBytes(out.toByteArray(), false);
        toWrite.clear();
    }

    public ArrayList<Object> readFile(FileHandle fileHandle) {
        InputStream inputStream;

        if (PROCESS_COMPRESSED_FILE_IO) {
            inputStream = new InflaterInputStream(fileHandle.read());
        } else {
            inputStream = fileHandle.read();
        }

        Input input = new Input(inputStream);
        Object object;
        LogManager.log(IOManager.class, Application.LOG_DEBUG, "(readFile) Try to read from file -> " + fileHandle.path());
        ArrayList<Object> result = new ArrayList<>();
        while (!input.eof()) {
            object = serializer.readClassAndObject(input);
            result.add(object);
        }

        return result;
    }

    public  ArrayList<Object> readFile(FileHandle fileHandle, Class clazz) {
        InputStream inputStream;

        if (PROCESS_COMPRESSED_FILE_IO) {
            inputStream = new InflaterInputStream(fileHandle.read());
        } else {
            inputStream = fileHandle.read();
        }

        Input input = new Input(inputStream);
        Object object;
        LogManager.log(IOManager.class, Application.LOG_DEBUG, "(readFile) Try to read from file -> " + fileHandle.path());
        ArrayList<Object> result = new ArrayList<>();
        while (!input.eof()) {
            object = serializer.readObject(input, clazz);
            result.add(object);
        }

        return result;
    }

    public void dispose() {
        serializer = null;
        toWrite.clear();
        toWrite = null;
    }
}
