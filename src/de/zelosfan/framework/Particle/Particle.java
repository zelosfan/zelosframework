package de.zelosfan.framework.Particle;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.*;
import de.zelosfan.framework.Interpolation.TweenHandler;
import de.zelosfan.framework.Rendering.Renderable;
import de.zelosfan.framework.Util.RandomUtil;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 21.05.13
 * Time: 09:57
 */

public class Particle  implements Pool.Poolable, Renderable {
    public TweenHandler positions;
    public ParticleDefinition typ;
    String textureName;
    public boolean alive = false;
    public int lifetime;

    float posX;
    float posY;
    float posZ;
    float sizeX;
    float sizeY;
    float rotation;

//    public final static ObjectMap<String, ParticleDefinition> particleDefinitionList = new ObjectMap<>();

//    public static void loadParticleDefinitions(String filepath) {
//        JsonValue value = new JsonReader().parse(Gdx.files.internal(filepath)).get("particleDefinitions");
//        for (JsonValue entry = value.child(); entry != null; entry = entry.next()) {
//            particleDefinitionList.put(entry.getString("uuid"),
//                    new ParticleDefinition(entry.getString("texturename"), entry.getBoolean("useAnimation"), entry.getInt("sizeX"), entry.getInt("sizeXvariance"), entry.getInt("sizeY"), entry.getInt("sizeYvariance"),
//                            entry.getInt("lifetime"), entry.getInt("lifetimeVariance"), entry.getInt("rotation"), entry.getInt("rotationVariance")));
//        }
//    }

    public Particle() {
    }

    @Override
    public TextureRegion getTexture() {
        return typ.getTexture(textureName);
    }

    public float getX() {
        return positions.get("X") + posX;
    }
    public float getY() {
        return positions.get("Y") + posY;
    }

    public float getZ() {
        return positions.get("Z") + posZ;
    }

    @Override
    public float getSizeX() {
        return positions.get("sizeX") + sizeX;
    }

    @Override
    public float getSizeY() {
        return positions.get("sizeY") + sizeY;
    }

    @Override
    public float getRotation() {
        return positions.get("rotation") + rotation;
    }

    public float getAlpha() {
        return positions.get("alpha");
    }

    public void tick() {
        if (alive) {
            positions.tick();
            lifetime--;

            if (lifetime <= 0) {
                alive = false;
            }
        }
    }

    public void initialize(ParticleDefinition definition, float posX, float posY) {
        positions = new TweenHandler();

        typ = definition;
        textureName = definition.getTextureName();

        this.posX = posX;
        this.posY = posY;
        this.rotation = definition.rotation;

        sizeX = definition.sizeX;
        sizeY = definition.sizeY;
     /*   if (definition.sizeXvariance == definition.sizeYvariance) {
            int variance = definition.sizeXvariance / 2 - RandomUtil.getNanoRandom().nextFloat() * RandomUtil.getNanoRandom().nextInt(definition.sizeXvariance);
            sizeX = definition.sizeX + variance;
            sizeY = definition.sizeY + variance;
        } else {
            if (definition.sizeXvariance > 0) {
                sizeX = definition.sizeX + (RandomUtil.getNanoRandom().nextInt(definition.sizeXvariance)) - definition.sizeXvariance / 2;
            } else {
                sizeX = definition.sizeX;
            }
            if (definition.sizeYvariance > 0) {
               sizeY = definition.sizeY - definition.sizeYvariance / 2 + RandomUtil.getNanoRandom().nextInt(definition.sizeYvariance);
            } else {
                sizeY = definition.sizeY;
            }
        }*/

        if (definition.rotationVariance > 0) {
            rotation = definition.rotation - definition.rotationVariance / 2 + RandomUtil.getNanoRandom().nextInt(definition.rotationVariance);
        } else {
            rotation = definition.rotation;
        }

        if (definition.lifetimeVariance > 0) {
            this.lifetime = definition.lifetime - definition.lifetimeVariance / 2 + RandomUtil.getNanoRandom().nextInt(definition.lifetimeVariance);
        } else {
            this.lifetime = definition.lifetime;
        }

        positions.add("X", 0, 0, 0, Interpolation.circle);
        positions.add("Y", 0, 0, 0, Interpolation.circle);
        positions.add("Z", 0, 0, 0, Interpolation.circle);
        positions.add("sizeX", 0, 0, 0, Interpolation.circle);
        positions.add("sizeY", 0, 0, 0, Interpolation.circle);
        positions.add("rotation", 0, 0, 0, Interpolation.circle);
        positions.add("alpha", 1, 1, 0, Interpolation.circle);

        alive = true;

        definition.setTweens(this);
    }

    @Override
    public void reset() {
        alive = false;
        positions.removeAll();
        textureName = "";
        lifetime = 0;

        posX = 0;
        posY = 0;
        posZ = 0;
        sizeX = 0;
        sizeY = 0;
        rotation = 0;
    }
}
