package de.zelosfan.framework.Particle;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.zelosfan.framework.Util.RandomUtil;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 21.05.13
 * Time: 12:53
 */
public abstract class ParticleDefinition {
    public final float sizeX;
    public final float sizeY;
    public final String[] texture;
    public final boolean animation;
    public final float sizeXvariance;
    public final float sizeYvariance;
    public final int lifetime;
    public final int lifetimeVariance;
    public final int rotation;
    public final int rotationVariance;
    public final float z;

    public ParticleDefinition(boolean _useAnimation, float sizeX, float _z, float sizeXvariance, float sizeY, float sizeYvariance, int lifetime, int lifetimeVariance, int rotation, int rotatationVariance, String... texture) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.texture = texture;
        this.animation = _useAnimation;
        this.sizeXvariance = sizeXvariance;
        this.sizeYvariance = sizeYvariance;
        this.lifetime = lifetime;
        this.lifetimeVariance = lifetimeVariance;
        this.rotation = rotation;
        this.rotationVariance = rotatationVariance;
        z = _z;
    }

    public String getTextureName() {
        return texture[RandomUtil.getNanoRandom().nextInt(texture.length)];
    }

    public abstract void setTweens(Particle particle);

    public abstract TextureRegion getTexture(String textureName);
}
