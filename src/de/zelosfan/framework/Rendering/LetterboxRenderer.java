package de.zelosfan.framework.Rendering;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 19.05.13
 * Time: 13:09
 */
public interface LetterboxRenderer {     //use 9Patches for best results
    void renderLeftLetterbox(Rendermanager manager);

    void renderRightLetterbox(Rendermanager manager);

    void renderTopLetterbox(Rendermanager manager);

    void renderBottomLetterbox(Rendermanager manager);
}
