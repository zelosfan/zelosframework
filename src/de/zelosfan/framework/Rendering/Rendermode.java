package de.zelosfan.framework.Rendering;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 28.05.13
 * Time: 06:15
 */
public enum Rendermode {
    NORMAL, WORLD, STRETCHED, PERSPECTIVE
}
