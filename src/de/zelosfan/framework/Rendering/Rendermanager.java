package de.zelosfan.framework.Rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import de.zelosfan.framework.GameState.GameStateManager;
import de.zelosfan.framework.Physics.PhysicsManager;
import de.zelosfan.framework.Particle.Particle;


/**
 * User: Simon "Zelosfan" Herfert
 * Date: 19.05.13
 * Time: 09:39
 */
public class Rendermanager implements Disposable {

    public final SpriteBatch spriteBatch;
    public final Camera camera;
    public Camera secondaryCamera;

    public Camera currentCamera;

    private String currentShader = "";

    public final Rendermode rendermode;

    public static float REALX_TO_VIRTUALX_RATIO, REALY_TO_VIRTUALY_RATIO;

    public static int FIELD_OF_VIEW = 67;

    public static float VIRTUAL_WIDTH;
    public static float VIRTUAL_HEIGHT;

    public static float ASPECT_RATIO;
    public static float ASPECT_RATIO_Y;

    public static float CURRENT_CAMPOS_X;
    public static float CURRENT_CAMPOS_Y;

    private boolean swapped = false;

    ObjectMap<String, ShaderProgram> shaderList;

    private Vector3 savedPos = Vector3.Zero;
    private Vector3 savedLookat = Vector3.Zero;

    public Rendermanager(Rendermode rendermode, int virtualWidth, int virtualHeight, int width, int height, ObjectMap<String, ShaderProgram> shaderList) {
        this.rendermode = rendermode;
        spriteBatch = new SpriteBatch();

        switch (rendermode) {
            case NORMAL:
                camera = new OrthographicCamera(width, height);
                OrthographicCamera oc = (OrthographicCamera) camera;
                oc.setToOrtho(false, width, height);
                Rendermanager.VIRTUAL_HEIGHT = virtualHeight;
                Rendermanager.VIRTUAL_WIDTH = virtualWidth;
                break;
            case WORLD:
                camera = new OrthographicCamera(virtualWidth, virtualHeight);
                OrthographicCamera oc2 = (OrthographicCamera) camera;
                oc2.setToOrtho(false, virtualWidth, virtualHeight);
                Rendermanager.VIRTUAL_HEIGHT = virtualHeight;
                Rendermanager.VIRTUAL_WIDTH = virtualWidth;
                break;
            case PERSPECTIVE:
                camera = new PerspectiveCamera(FIELD_OF_VIEW, width, height);
                camera.update();
                Rendermanager.VIRTUAL_HEIGHT = virtualHeight;
                Rendermanager.VIRTUAL_WIDTH = virtualWidth;
                break;

            default:
                camera = new OrthographicCamera(width, height);
                Rendermanager.VIRTUAL_HEIGHT = virtualHeight;
                Rendermanager.VIRTUAL_WIDTH = virtualWidth;

        }
        this.shaderList = shaderList;

        REALX_TO_VIRTUALX_RATIO = VIRTUAL_WIDTH / width;
        REALY_TO_VIRTUALY_RATIO = VIRTUAL_HEIGHT / height;
        ASPECT_RATIO = VIRTUAL_WIDTH / VIRTUAL_HEIGHT;
        ASPECT_RATIO_Y = VIRTUAL_HEIGHT / VIRTUAL_WIDTH;

        currentCamera = camera;

        resize(width, height);
    }

    //MAIN CALLS
    @Deprecated
    public void render() {
        Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        update();

        begin();
        GameStateManager.getGameStateManager().render();
        end();
    }

    public void clearScreen() {
        Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    public void update() {
        currentCamera.update();
        spriteBatch.setProjectionMatrix(currentCamera.combined);

        CURRENT_CAMPOS_X = currentCamera.position.x - currentCamera.viewportWidth / 2;
        CURRENT_CAMPOS_Y = currentCamera.position.y - currentCamera.viewportHeight / 2;
    }

    public void resize(int width, int height) {
        switch (rendermode) {
            case NORMAL:
                camera.viewportWidth = width;
                camera.viewportHeight = height;
                break;

            case STRETCHED:
                camera.viewportWidth = width;
                camera.viewportHeight = height;
                break;

            case WORLD:
                VIRTUAL_WIDTH = REALX_TO_VIRTUALX_RATIO * width;
                VIRTUAL_HEIGHT = REALY_TO_VIRTUALY_RATIO * height;
                OrthographicCamera oc = (OrthographicCamera) camera;
                oc.setToOrtho(false, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
                break;

            case PERSPECTIVE:
                camera.viewportWidth = width;
                camera.viewportHeight = height;
                camera.update();
                break;

            default:
                camera.viewportWidth = width;
                camera.viewportHeight = height;
                break;
        }
        camera.update();
    }

    public void setSecondaryCamera(Camera camera) {
        secondaryCamera = camera;
    }

    public void quickTranslate(Vector3 lookat, Vector3 position) {
        savedPos = currentCamera.position.cpy();
        savedLookat = currentCamera.direction.cpy();

        if (!position.equals(Vector3.Zero)) {
            currentCamera.position.x = position.x;
            currentCamera.position.y = position.y;
            currentCamera.position.z = position.z;
        }

        if (!lookat.equals(Vector3.Zero)) {
            currentCamera.lookAt(lookat);
        }
        update();
    }

    public void revertTranslate() {
        currentCamera.position.x = savedPos.x;
        currentCamera.position.y = savedPos.y;
        currentCamera.position.z = savedPos.z;

        currentCamera.direction.x = savedLookat.x;
        currentCamera.direction.y = savedLookat.y;
        currentCamera.direction.z = savedLookat.z;
        update();
    }

    public void swapCamera(boolean main) {
        boolean drawing = spriteBatch.isDrawing();
        if (drawing) {
            spriteBatch.end();
        }
        if (main)  {
            if (currentCamera.equals(camera)) {
                swapped = false;
            } else {
                currentCamera = camera;
                swapped = true;
            }
        } else {
            if (currentCamera.equals(secondaryCamera)) {
                swapped = false;
            } else {
                currentCamera = secondaryCamera;
                swapped = true;
            }
        }
        update();
        if (drawing) {
            spriteBatch.begin();
        }
    }

    public void swapBack() {
        if (swapped) {
            if (currentCamera.equals(camera)) {
                currentCamera = secondaryCamera;
            } else {
                currentCamera = camera;
            }
        }
    }

//    public static float getScale() {
//        return scale;
//    }
    //#######################

    //INDEPENDENT TEXTUREREGION DRAW CALLS
    public void drawIndependentRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float r, float g, float b, float a) {
        spriteBatch.setColor(r, g, b, a);
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height, 1, 1, rotation);
        spriteBatch.setColor(Color.WHITE);
    }

    public void drawIndependentRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, Color color, float scale) {
        spriteBatch.setColor(color);
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height, scale, scale, rotation);
        spriteBatch.setColor(Color.WHITE);
    }

    public void drawIndependentRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height, 1, 1, rotation);
    }

    public void drawIndependentYMinusHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y - VIRTUAL_WIDTH * width * height, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height, 1, 1, rotation);
    }

    public void drawIndependentAbsoluteHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * height, 1, 1, rotation);
    }

    public void drawIndependentRaw(TextureRegion textureRegion, float x, float y, float width, float height, float rotation) {
        spriteBatch.draw(textureRegion, x + CURRENT_CAMPOS_X, y + CURRENT_CAMPOS_Y, width * 0.5f, height * 0.5f, width, height, 1, 1, rotation);
    }

    public void drawIndependentRaw(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float r, float g, float b, float a) {
        spriteBatch.setColor(r, g, b, a);
        spriteBatch.draw(textureRegion, x + CURRENT_CAMPOS_X, y + CURRENT_CAMPOS_Y, width * 0.5f, height * 0.5f, width, height, 1, 1, rotation);
        spriteBatch.setColor(Color.WHITE);
    }
    //######################



    //########################

    //SPRITEBATCH WRAPPER
    public void begin() {

        spriteBatch.begin();
    }

    public void end() {
        spriteBatch.end();
    }

    public void flush() {
        spriteBatch.flush();
    }
    //###################

    //COLOR STUFF
    public void setColor(float r, float g, float b, float a) {
        spriteBatch.setColor(r, g, b, a);
    }

    public void setColor(Color color) {
        spriteBatch.setColor(color);
    }

    public Color getColor() {
        return spriteBatch.getColor();
    }
    //##########################

    //SHADER STUFF
    public void setShaderUniform(String shader, String name, float v) {
        shaderList.get(shader).begin();
        shaderList.get(shader).setUniformf(name, v);
        shaderList.get(shader).end();
    }

    public void setShader(String shader) {
        if (shader.equals(currentShader)) return;

        if (shaderList.containsKey(shader)) {
            spriteBatch.setShader(shaderList.get(shader));
            currentShader = shader;
        }
    }

    public void clearShader() {
        if (currentShader.equals("")) return;

        spriteBatch.setShader(null);
        currentShader = "";
    }
    //#########################

    //DRAW TEXT
    public void drawText(BitmapFont font, String text, float x, float y, String shader, Color color) {
        drawAbsoluteText(font, text, x * VIRTUAL_WIDTH, y * VIRTUAL_HEIGHT, shader, color);
    }

    public void drawAbsoluteText(BitmapFont font, String text, float x, float y, String shader, Color color) {
        setShader(shader);
        font.setColor(color);
        font.draw(spriteBatch, text, x, y);
        font.setColor(Color.WHITE);
        clearShader();
    }

    public void drawWrapped(BitmapFont font, String text, float x, float y, float width, String shader, Color color) {
        setShader(shader);
        font.setColor(color);
        font.draw(spriteBatch, text, x, y, width, 1, true);
        font.setColor(Color.WHITE);
        clearShader();
    }

    public void drawIndependentWrapped(BitmapFont font, String text, float x, float y, float width, int halign, String shader, Color color) {
        setShader(shader);
        font.setColor(color);
        font.draw(spriteBatch, text, x + CURRENT_CAMPOS_X, y + CURRENT_CAMPOS_Y, width, halign, true);
        font.setColor(Color.WHITE);
        clearShader();
    }

    public void drawText(BitmapFont font, String text, float x, float y) {
        drawText(font, text, x, y, "font", Color.BLACK);
    }

    //DRAW INDEPENDENT TEXT
    public void drawIndependentText(BitmapFont font, String shader, String text, float x, float y, float r, float g, float b, float a) {
        setShader(shader);
        font.setColor(r, g, b, a);
        font.draw(spriteBatch, text, VIRTUAL_WIDTH * x + CURRENT_CAMPOS_X, VIRTUAL_HEIGHT * y + CURRENT_CAMPOS_Y);
        clearShader();
    }

    public void drawIndependentText(BitmapFont font, String shader, String text, float x, float y) {
        drawIndependentText(font, shader, text, x, y, Color.BLACK);
    }

    @Deprecated
    public void drawIndependentCenteredText(BitmapFont font, String shader, String text, float x, float y) {
        drawIndependentCenteredText(font, shader, text, x, y, 1, 1, 1, 1);
    }

    @Deprecated
    public void drawIndependentCenteredText(BitmapFont font, String shader, String text, float x, float y, float r, float g, float b, float a) {
    //    drawIndependentText(font, shader, text, x - (font.g(text).width * 0.5f) / VIRTUAL_WIDTH, y, r, g, b, a);
    }

    public void drawIndependentText(BitmapFont font, String shader, String text, float x, float y, Color color) {
        drawIndependentText(font, shader, text, x, y, color.r, color.g, color.b, color.a);
    }

    public void drawIndependentText(BitmapFont font, String text, float x, float y) {
        drawIndependentText(font, "font", text, x, y);
    }
    //######################

    //BASIC TEXTUREREGION RENDER CALLS
    public void rawDraw(TextureRegion textureRegion, int x, int y, int width, int height, float rotation, float r, float g , float b, float a) {
        spriteBatch.setColor(r, g, b, a);
        spriteBatch.draw(textureRegion, x, y, width * 0.5f, height * 0.5f, width, height, 1, 1, rotation);
        spriteBatch.setColor(Color.WHITE);
    }

    public void rawDraw(TextureRegion textureRegion, int x, int y, int width, int height, float rotation) {
        spriteBatch.draw(textureRegion, x, y, width * 0.5f, height * 0.5f, width, height, 1, 1, rotation);
    }

    public void rawDraw(TextureRegion textureRegion, Rectangle drawRegion) {
        spriteBatch.draw(textureRegion, drawRegion.x, drawRegion.y, drawRegion.width, drawRegion.height);
    }

    public void rawDrawCentered(TextureRegion textureRegion, int x, int y, int width, int height) {
        spriteBatch.draw(textureRegion, x - width / 2f, y - height / 2f, width, height);
    }

    public void rawDraw(TextureRegion textureRegion, int x, int y, int width, int height) {
        spriteBatch.draw(textureRegion, x, y, width, height);
    }

    public void drawAbsoluteHeight(TextureRegion textureRegion, float x, float y, float width, float height) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x, VIRTUAL_HEIGHT * y, VIRTUAL_WIDTH * width, VIRTUAL_HEIGHT * height);
    }

    public void drawAbsoluteHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float r, float g, float b, float a) {
        spriteBatch.setColor(r, g, b, a);
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x, VIRTUAL_HEIGHT * y, VIRTUAL_WIDTH * width * 0.5f, VIRTUAL_HEIGHT * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_HEIGHT * height, 1, 1, rotation, true);
        spriteBatch.setColor(Color.WHITE);
    }

    public void drawRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, Color color) {
        spriteBatch.setColor(color);
        spriteBatch.draw(textureRegion, Math.round(VIRTUAL_WIDTH * x), Math.round(VIRTUAL_HEIGHT * y), Math.round(VIRTUAL_WIDTH * width) * 0.5f, Math.round(VIRTUAL_HEIGHT * height) * 0.5f, Math.round(VIRTUAL_WIDTH * width), Math.round(VIRTUAL_HEIGHT * height), 1, 1, rotation);
        spriteBatch.setColor(Color.WHITE);
    }

    public void drawRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float r, float g, float b, float a) {
        spriteBatch.setColor(r, g, b, a);
        spriteBatch.draw(textureRegion, Math.round(VIRTUAL_WIDTH * x), Math.round(VIRTUAL_HEIGHT * y), Math.round(VIRTUAL_WIDTH * width) * 0.5f, Math.round(VIRTUAL_HEIGHT * height) * 0.5f, Math.round(VIRTUAL_WIDTH * width), Math.round(VIRTUAL_HEIGHT * height), 1, 1, rotation);
        spriteBatch.setColor(Color.WHITE);
    }

    public void drawForOutline(TextureRegion textureRegion, float x, float y, float width, float height, float rotation, float size) {
        spriteBatch.draw(textureRegion, Math.round(VIRTUAL_WIDTH * x), Math.round(VIRTUAL_HEIGHT * y), Math.round(VIRTUAL_WIDTH * width) * 0.5f, Math.round(VIRTUAL_HEIGHT * height) * 0.5f, Math.round(VIRTUAL_WIDTH * width), Math.round(VIRTUAL_HEIGHT * height), size, size, rotation);
    }

    public void drawRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x, VIRTUAL_HEIGHT * y, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height);
    }

    public void drawRelativeHeight(TextureRegion textureRegion, float x, float y, float width, float height, float rotation) {
        spriteBatch.draw(textureRegion, VIRTUAL_WIDTH * x, VIRTUAL_HEIGHT * y, VIRTUAL_WIDTH * width * 0.5f,  VIRTUAL_WIDTH * width * height * 0.5f, VIRTUAL_WIDTH * width, VIRTUAL_WIDTH * width * height, 1, 1, rotation);
    }
    //##########################

    //ANIMATIONOBJECT RENDER CALLS
   /* public void drawAnimationObject(AnimationObject animationObject) {
        if (animationObject.isVisible()) {
            Array<Slot> drawOrder = animationObject.getSkeleton().getDrawOrder();
            boolean additive = false;
            for (int i = 0, n = drawOrder.size; i < n; i++) {
                Slot slot = drawOrder.get(i);
                Attachment attachment = slot.getAttachment();
                if (attachment instanceof RegionAttachment) {
                    RegionAttachment regionAttachment = (RegionAttachment) attachment;
                    regionAttachment.updateVertices(slot);
                    float[] vertices = regionAttachment.getVertices().clone();
                    if (slot.getData().getAdditiveBlending() != additive) {
                        additive = !additive;
                        if (additive) {
                            spriteBatch.setBlendFunction(GL30.GL_SRC_ALPHA, GL30.GL_ONE);
                        } else {
                            spriteBatch.setBlendFunction(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
                        }
                    }
//                    Gdx.app.debug("x1", "" + vertices[X1]);
//                    Gdx.app.debug("x2", "" + vertices[X2]);
//                    Gdx.app.debug("x3", "" + vertices[X3]);
//                    Gdx.app.debug("x4", "" + vertices[X4]);
//                    Gdx.app.debug("y1", "" + vertices[Y1]);
//                    Gdx.app.debug("y2", "" + vertices[Y2]);
//                    Gdx.app.debug("y3", "" + vertices[Y3]);
//                    Gdx.app.debug("y4", "" + vertices[Y4]);
                    vertices[X1] = VIRTUAL_WIDTH * (( animationObject.getPositionX()) * PhysicsManager.BOX2D_TO_GAME_X) + vertices[X1];
                    vertices[X2] = VIRTUAL_WIDTH * (( animationObject.getPositionX()) * PhysicsManager.BOX2D_TO_GAME_X) + vertices[X2];
                    vertices[X3] = VIRTUAL_WIDTH * (( animationObject.getPositionX()) * PhysicsManager.BOX2D_TO_GAME_X) + vertices[X3];
                    vertices[X4] = VIRTUAL_WIDTH * (( animationObject.getPositionX()) * PhysicsManager.BOX2D_TO_GAME_X) + vertices[X4];
                    vertices[Y1] = VIRTUAL_HEIGHT * (( animationObject.getPositionY()) * PhysicsManager.BOX2D_TO_GAME_Y) + vertices[Y1];
                    vertices[Y2] = VIRTUAL_HEIGHT * (( animationObject.getPositionY()) * PhysicsManager.BOX2D_TO_GAME_Y) + vertices[Y2];
                    vertices[Y3] = VIRTUAL_HEIGHT * (( animationObject.getPositionY()) * PhysicsManager.BOX2D_TO_GAME_Y) + vertices[Y3];
                    vertices[Y4] = VIRTUAL_HEIGHT * (( animationObject.getPositionY()) * PhysicsManager.BOX2D_TO_GAME_Y) + vertices[Y4];
                    spriteBatch.draw(regionAttachment.getRegion().getTexture(), vertices, 0, vertices.length);
                }
            }
            if (additive) spriteBatch.setBlendFunction(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
        }
    } */
    //##########################

    //RENDERABLE RENDER CALLS
    public void rawDraw(Renderable drawable) {
        setColor(0, 0, 0, drawable.getAlpha());
        rawDraw(drawable.getTexture(), (int) drawable.getX(), (int) drawable.getY(), (int) drawable.getSizeX(), (int) drawable.getSizeY(), drawable.getRotation());
        setColor(Color.WHITE);
    }

    public void drawAbsolutHeight(Renderable drawable) {
        drawAbsoluteHeight(drawable.getTexture(), drawable.getX(), drawable.getY(), drawable.getSizeX(), drawable.getSizeY(), drawable.getRotation(), 1, 1, 1, 1);
    }

    public void drawRelativeHeight(Renderable drawable) {
        drawAbsoluteHeight(drawable.getTexture(), drawable.getX(), drawable.getY(), drawable.getSizeX(), drawable.getSizeX() * drawable.getSizeY(), drawable.getRotation(), 1, 1, 1, 1);
    }
    //##########################

    //PARTICLE RENDER CALLS
    public void render(Particle particle) {
        if (particle.alive) {
            float z = particle.getZ();
            if (z > 0) {
                drawAbsoluteHeight(particle.getTexture(), (particle.getX() * PhysicsManager.BOX2D_TO_GAME_X) - (particle.getSizeX() * PhysicsManager.BOX2D_TO_GAME_X) * (0.5f * z), (particle.getY() * PhysicsManager.BOX2D_TO_GAME_Y) - (particle.getSizeY() * PhysicsManager.BOX2D_TO_GAME_Y) * (0.5f * z), (particle.getSizeX() * PhysicsManager.BOX2D_TO_GAME_X * (1 + z)), (particle.getSizeY() * PhysicsManager.BOX2D_TO_GAME_Y * (1 + z)), particle.getRotation(), 1, 1, 1, particle.getAlpha());
            } else {
                drawAbsoluteHeight(particle.getTexture(), (particle.getX() * PhysicsManager.BOX2D_TO_GAME_X), (particle.getY() * PhysicsManager.BOX2D_TO_GAME_Y), (particle.getSizeX() * PhysicsManager.BOX2D_TO_GAME_X), (particle.getSizeY() * PhysicsManager.BOX2D_TO_GAME_Y ), particle.getRotation(), 1, 1, 1, particle.getAlpha());
            }
        }
    }
    //########################

    @Override
    public void dispose() {
        spriteBatch.dispose();
    }
}
