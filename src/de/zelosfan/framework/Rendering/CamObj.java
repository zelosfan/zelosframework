package de.zelosfan.framework.Rendering;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 06.06.13
 * Time: 14:15
 */
public class CamObj {
    public OrthographicCamera camera;
    public float ratioToMainX;
    public float ratioToMainY;

    public CamObj(OrthographicCamera _camera, float _ratioToMainX, float _ratioToMainY) {
        camera = _camera;
        ratioToMainX = _ratioToMainX;
        ratioToMainY = _ratioToMainY;
    }
}
