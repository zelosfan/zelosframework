package de.zelosfan.framework.Rendering;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import de.zelosfan.framework.Util.RectangleM;

import java.util.ArrayList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 06.06.13
 * Time: 14:14
 */
public class CameraManager {
    private Camera mainCam;
    private RectangleM cameraBorder;

    private boolean isRestrictedToBorder = false;
    private ArrayList<CamObj> childCams;

    public CameraManager(Camera _mainCam) {
        childCams = new ArrayList<>();
        mainCam = _mainCam;
    }

    public Camera getMainCam() {
        return mainCam;
    }

    public void setBorderRestriction(RectangleM border) {
        cameraBorder = border;
    }

    public RectangleM getBorderRestriction() {
        return cameraBorder;
    }

    public void setIsBorderRestricted(boolean val) {
        isRestrictedToBorder = val;
    }

    public boolean isBorderRestricted() {
        return isRestrictedToBorder;
    }

    public float getPositionCenterX() {
        return mainCam.position.x + mainCam.viewportWidth / 2;
    }

    public float getPositionCenterY() {
        return mainCam.position.y + mainCam.viewportHeight / 2;
    }

    public Vector2 getPositionCenter() {
        return new Vector2(mainCam.position.x + mainCam.viewportWidth / 2, mainCam.position.y + mainCam.viewportHeight / 2);
    }

    public Vector2 getPosition() {
        return new Vector2(mainCam.position.x, mainCam.position.y);
    }

    public float getPositionX() {
        return mainCam.position.x;
    }

    public float getPositionY() {
        return mainCam.position.y;
    }

    public void addChildCam(OrthographicCamera _childCam, float _ratioX, float _ratioY) {
        childCams.add(new CamObj(_childCam, _ratioX, _ratioY));
    }

    public void updateRatios(OrthographicCamera _camera, float _ratioX, float _ratioY) {
        for (CamObj camObj: childCams) {
            if (camObj.camera.equals(_camera)) {
                camObj.ratioToMainX = _ratioX;
                camObj.ratioToMainY = _ratioY;
            }
        }
        updateChilds();
    }

    public void updateChilds() {
        for (CamObj camObj : childCams) {
            camObj.camera.position.x = mainCam.position.x * camObj.ratioToMainX;
            camObj.camera.position.y = mainCam.position.y * camObj.ratioToMainY;
            camObj.camera.update();
        }
    }

    private float getCorrectedCameraX(float x) {
        if (!isRestrictedToBorder) return x;

        /*if (x < cameraBorder.x) return cameraBorder.x;
        if (x + mainCam.viewportWidth / 2 > cameraBorder.getX() + cameraBorder.getWidth()) {
            return cameraBorder.getX() + cameraBorder.getWidth() - mainCam.viewportWidth / 2;
        }*/

        if (x - mainCam.viewportWidth / 2 < cameraBorder.x) return cameraBorder.x + mainCam.viewportWidth / 2;
        if (x + mainCam.viewportWidth / 2 > cameraBorder.getWidth()) {
            return cameraBorder.getWidth() - mainCam.viewportWidth / 2;
        }
        return x;
    }

    private float getCorrectedCameraY(float y) {
        if (!isRestrictedToBorder) return y;

      /*  if (y < cameraBorder.y) return cameraBorder.y;
        if (y + mainCam.viewportHeight / 2 > cameraBorder.getY() + cameraBorder.getHeight()) {
            return cameraBorder.getY() + cameraBorder.getHeight() - mainCam.viewportHeight / 2;
        } */

        if (y - mainCam.viewportHeight / 2 < cameraBorder.y) return cameraBorder.y + mainCam.viewportHeight / 2;
        if (y + mainCam.viewportHeight / 2 > cameraBorder.getHeight()) {
            return cameraBorder.getHeight() - mainCam.viewportHeight / 2;
        }

        return y;
    }

    public void setMain(float x, float y) {
        mainCam.position.x = getCorrectedCameraX(x);
        mainCam.position.y = getCorrectedCameraY(y);

        mainCam.update();
        updateChilds();
    }

    @Deprecated
    public void setMainCenter(float x, float y) {
        setMain(x + mainCam.viewportWidth / 2, y + mainCam.viewportHeight / 2);
    }

    public void addMain(float x, float y, float z) {
        mainCam.translate(x, y, z);
        mainCam.update();
        updateChilds();
    }


}
