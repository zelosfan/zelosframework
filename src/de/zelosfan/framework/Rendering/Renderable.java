package de.zelosfan.framework.Rendering;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 12.06.13
 * Time: 18:45
 */
public interface Renderable {
    float getX();
    float getY();

    float getSizeX();
    float getSizeY();

    float getRotation();
    float getAlpha();

    TextureRegion getTexture();
}
