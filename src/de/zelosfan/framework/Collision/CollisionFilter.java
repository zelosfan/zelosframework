package de.zelosfan.framework.Collision;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 01.10.13
 * Time: 16:55
 *
 *
 * Should be used together with the CollisionHandler
 * Needs a valid object implementing Collisionable in the fixture's body userdata
 *
 * Can be set via Physicsmanager constructor or with Box2DWorld.setContactFilter
 */
public class CollisionFilter implements com.badlogic.gdx.physics.box2d.ContactFilter {
    @Override
    public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {
        if (fixtureA.getBody().getUserData() instanceof Collisionable && fixtureB.getBody().getUserData() instanceof Collisionable) {
            Collisionable a = (Collisionable) fixtureA.getBody().getUserData();
            Collisionable b = (Collisionable) fixtureB.getBody().getUserData();

            return a.canCollide(b);
        }

        return true;
    }
}
