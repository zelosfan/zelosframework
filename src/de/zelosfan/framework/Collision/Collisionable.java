package de.zelosfan.framework.Collision;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 01.10.13
 * Time: 16:57
 */
public interface Collisionable {
    CollisionMask getCollisionMask(); // CollisionMasks

    //Should be resolved by the CollisionMasks procedures "canCollide" and "doesResolve" but is interfaced for overwrite convenience
    boolean canCollide(Collisionable object); //default: return getCollisionMask().canCollide(object.getCollisionMask());
    boolean doesResolveCollision(Collisionable object); //default: return getCollisionMask().doesResolve(object.getCollisionMask());

    //Events
    void onCollide(Collisionable object);
    void onCollisionEnd(Collisionable object);
}
