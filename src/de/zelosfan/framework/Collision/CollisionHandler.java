package de.zelosfan.framework.Collision;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 01.10.13
 * Time: 17:34
 *
 *
 * Should be used together with the CollisionFilter
 * Needs a valid object implementing Collisionable in the fixture's body userdata
 *
 * Can be set via Physicsmanager constructor or with Box2DWorld.setContactListener
 */
public class CollisionHandler implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        if (contact.getFixtureA() != null && contact.getFixtureB() != null) {
            if (contact.getFixtureA().getBody().getUserData() instanceof Collisionable && contact.getFixtureB().getBody().getUserData() instanceof Collisionable) {
                Collisionable a = (Collisionable) contact.getFixtureA().getBody().getUserData();
                Collisionable b = (Collisionable) contact.getFixtureB().getBody().getUserData();
                a.onCollide(b);
                b.onCollide(a);
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        if (contact.getFixtureA() != null && contact.getFixtureB() != null) {
            if (contact.getFixtureA().getBody().getUserData() instanceof Collisionable && contact.getFixtureB().getBody().getUserData() instanceof Collisionable) {
                Collisionable a = (Collisionable) contact.getFixtureA().getBody().getUserData();
                Collisionable b = (Collisionable) contact.getFixtureB().getBody().getUserData();
                a.onCollisionEnd(b);
                b.onCollisionEnd(a);
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        if (contact.getFixtureA() != null && contact.getFixtureB() != null) {
            if (contact.getFixtureA().getBody().getUserData() instanceof Collisionable && contact.getFixtureB().getBody().getUserData() instanceof Collisionable) {
                Collisionable a = (Collisionable) contact.getFixtureA().getBody().getUserData();
                Collisionable b = (Collisionable) contact.getFixtureB().getBody().getUserData();

                contact.setEnabled(a.doesResolveCollision(b));
            }
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
