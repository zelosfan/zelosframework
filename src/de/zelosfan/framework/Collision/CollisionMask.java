package de.zelosfan.framework.Collision;

import java.util.LinkedList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 01.10.13
 * Time: 17:01
 */
public class CollisionMask<T extends Enum<T>> {
    public final T collisionTyp;
    public final T[] filteredCollisionTypes; // Alle Typen mit denen eine Collision eingegangen werden kann / oder nicht
    public boolean collisionAdditive; // if the types in filteredCollisionTypes are additiv or subtractive
    public final LinkedList<T> unresolvedCollisionTypes; // Alle Typen die eine Collision eingehen können und deren Collision nicht aufgelöst wird -> unresolvedCollisionTypes ist eine Teilmenge von collsionTypes

    @SafeVarargs
    public CollisionMask(T _collisionTyp, boolean _additiveMask, T... _collisionMask) {
        collisionTyp = _collisionTyp;
        filteredCollisionTypes = _collisionMask;
        collisionAdditive = _additiveMask;
        unresolvedCollisionTypes = new LinkedList<>();
    }

    public CollisionMask addUnresolvedMask(T unresolvedTyp) {
        unresolvedCollisionTypes.add(unresolvedTyp);
        return this;
    }

    public boolean canCollide(CollisionMask collisionMask) { //checks both ways
        boolean sideA = !collisionMask.collisionAdditive;
        boolean sideB = !collisionAdditive;

        for (int i = 0; i < collisionMask.filteredCollisionTypes.length; i++) {
            if (collisionTyp.equals(collisionMask.filteredCollisionTypes[i])) {
                sideA = collisionMask.collisionAdditive;
            }
        }

        for (T filteredCollisionType : filteredCollisionTypes) {
            if (collisionMask.collisionTyp.equals(filteredCollisionType)) {
                sideB = collisionAdditive;
            }
        }

        return sideA && sideB;
    }

    public boolean doesResolve(CollisionMask collisionMask) { // checks both ways and is additive by both sides
        return !(unresolvedCollisionTypes.contains(collisionMask.collisionTyp) || collisionMask.unresolvedCollisionTypes.contains(collisionTyp));
    }

}
