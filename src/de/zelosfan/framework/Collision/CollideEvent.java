package de.zelosfan.framework.Collision;

/**
 * Created by Zelos on 13.06.2015.
 */
public interface CollideEvent {
    void onCollide(CollideEvent object);
    void onCollisionEnd(CollideEvent object);
}
