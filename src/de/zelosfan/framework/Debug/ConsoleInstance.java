package de.zelosfan.framework.Debug;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import de.zelosfan.framework.GameState.Instance;
import de.zelosfan.framework.Logging.LogManager;
import de.zelosfan.framework.Rendering.Rendermanager;
import de.zelosfan.framework.Service.ServiceLocator;
import de.zelosfan.framework.Timing.GametimeManager;

import java.util.ArrayList;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 06.09.13
 * Time: 08:33
 */
public class ConsoleInstance implements Instance {

    public static int BACKSPACE_REMOVETIME = 70;

    protected boolean enabled = false;
    protected boolean displayEnabled = false;

    protected int curFPS = 0;
    protected int curTPS = 0;

    protected BitmapFont font;
    protected ArrayList<String> consoleInputs = new ArrayList<>(10);
    protected String currentInput = "";

    protected int backspacePressedCounter = 1;
    protected boolean backspacePressed = false;

    protected Rendermanager rendermanager;

    public ConsoleInstance(BitmapFont _font, Rendermanager _rendermanager) {
        font = _font;

        rendermanager = _rendermanager;
    }

    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    public void executeCommand(String... command) {
        if (command.length < 1) return;

        switch (command[0]) {
            case "fps":
                respond("FPS: " + Gdx.graphics.getFramesPerSecond());
                break;
            case "tc":
                respond("TICKCOUNT: " + ServiceLocator.gametimeHandler.getTotalTickCount());
                break;
        }
    }

    public void respond(String response) {
        LogManager.log(ConsoleInstance.class, Application.LOG_DEBUG, "(ConsoleResponse) " + response);
        consoleInputs.add("!--> " + response);
    }

    public void render() {
       // Rendermanager.swapCamera(false);
        if (displayEnabled) {
            Color color = Color.GREEN;

            if (curFPS < 20) {
                color = Color.RED;
            } else if (curFPS < 30) {
                color = Color.ORANGE;
            } else if (curFPS < 50) {
                color = Color.YELLOW;
            }

            rendermanager.drawIndependentText(font, "fontOutline", "FPS: ", 0.01f, 0.95f, color);
            rendermanager.drawIndependentText(font, "fontOutline", "          " + curFPS, 0.01f, 0.95f);
        }


        //CONSOLE DRAW
        if (enabled) {

            for (int i = 0; i < Math.min(consoleInputs.size(), 9); i++) {
                rendermanager.drawIndependentText(font, "fontOutline", consoleInputs.get(consoleInputs.size() - 1 - i), 0.01f, 0.07f + 0.025f * (i + 1));
            }
            rendermanager.drawIndependentText(font, "fontOutline", "________________________________________", 0.01f, 0.09f);
            if (ServiceLocator.gametimeHandler.getTotalTickCount() % 120 < 60) {
                rendermanager.drawIndependentText(font, "fontOutline", currentInput, 0.01f, 0.06f);
            } else {
                rendermanager.drawIndependentText(font, "fontOutline", currentInput + "_", 0.01f, 0.06f);
            }
            rendermanager.drawIndependentText(font, "fontOutline", "________________________________________", 0.01f, 0.05f);

        }
        //######################
       // rendermanager.swapBack();
    }

    @Override
    public void tick() {
        if (displayEnabled) {
            if (ServiceLocator.gametimeHandler.getTotalTickCount() % 10 == 0) {
                curFPS = Gdx.graphics.getFramesPerSecond();
            }
            if (backspacePressed) {
                backspacePressedCounter++;
                if (backspacePressedCounter > BACKSPACE_REMOVETIME) {
                    if (currentInput.length() > 0) {
                        currentInput = currentInput.substring(0, currentInput.length() - 1);
                    }
                }
            }
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.F1:
                enabled = !enabled;
                if (enabled) displayEnabled = true;
                return true;

            case Input.Keys.F2:
                displayEnabled = !displayEnabled;
                return true;

            case Input.Keys.ENTER:
                if (enabled) {
                    LogManager.log(ConsoleInstance.class, Application.LOG_DEBUG, "(Console)" + currentInput);
                    consoleInputs.add(currentInput);
                    executeCommand(currentInput.split(" "));
                    currentInput = "";
                    return true;
                }
                break;

            case Input.Keys.SPACE:
                currentInput += " ";
                return true;

            case Input.Keys.BACKSPACE:
                if (enabled) {
                    backspacePressed = true;
                    if (currentInput.length() > 0) {
                        currentInput = currentInput.substring(0, currentInput.length() - 1);
                        return true;
                    }
                }
                break;

            case Input.Keys.UP:
                if (enabled) {
                    if (consoleInputs.size() > 0) {
                        currentInput = getLastCMD();
                        return true;
                    }
                }
                break;
        }

        return enabled;
    }

    public String getLastCMD() {
        for (int i = 0; i < consoleInputs.size(); i++) {
            if (!consoleInputs.get(consoleInputs.size() - 1 - i).startsWith("!")) {
                return consoleInputs.get(consoleInputs.size() - 1 - i);
            }
        }
        return "";
    }

    @Override
    public boolean keyUp(int keycode) {

        switch (keycode) {
            case Input.Keys.BACKSPACE:
                backspacePressed = false;
                backspacePressedCounter = 0;
                break;
        }

        return enabled;
    }

    @Override
    public boolean keyTyped(char character) {
        if (!enabled) return false;
        String check = "" + character;
        if (!check.matches("[0-9a-z.,_]")) return false;
        currentInput += character;
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
