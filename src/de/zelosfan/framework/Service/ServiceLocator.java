package de.zelosfan.framework.Service;

import de.zelosfan.framework.Audio.AudioEngine;
import de.zelosfan.framework.Audio.Audiomanager;
import de.zelosfan.framework.Audio.NullAudioManager;
import de.zelosfan.framework.IO.IOManager;
import de.zelosfan.framework.Timing.GametimeHandler;
import de.zelosfan.framework.Timing.GametimeManager;

/**
 * Created by Zelos on 12.04.2015.
 */
public class ServiceLocator {

    public static final AudioEngineType audioEngineTyp = AudioEngineType.LIBGDX_VANILLA;

    public static AudioEngine audioEngine;
    public static GametimeHandler gametimeHandler;
    public static IOManager ioManager;

    static {

        switch (audioEngineTyp) {
            case LIBGDX_VANILLA:
                audioEngine = new Audiomanager();
                break;

            case NONE:
                audioEngine = new NullAudioManager();
                break;
        }

        gametimeHandler = new GametimeManager();

        ioManager = new IOManager();
    }

    public static void dispose() {
        audioEngine.dispose();
        ioManager.dispose();
    }

}
