package de.zelosfan.framework.Util;


import org.junit.Assert;
import org.junit.Test;

/**
 * User: Simon "Zelosfan" Herfert
 * Date: 09.08.13
 * Time: 09:58
 */
public class RandomUtilTest {
    @Test
    public void testGetNanoRandom() throws Exception {
        Assert.assertNotSame(RandomUtil.getNanoRandom().nextInt(1234567), RandomUtil.getNanoRandom().nextInt(1234567));

    }

    @Test
    public void testGetIdRandom() throws Exception {
        Assert.assertNotSame(RandomUtil.getIdRandom("bla", 1).nextInt(100), RandomUtil.getIdRandom("bla", 2).nextInt(100));
        Assert.assertSame(RandomUtil.getIdRandom("bla", 1).nextInt(100), RandomUtil.getIdRandom("bla", 1).nextInt(100));
    }

    @Test
    public void testGetID() throws Exception {

    }

    @Test
    public void testUpdateID() throws Exception {

    }
}
